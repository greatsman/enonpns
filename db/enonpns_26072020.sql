--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.14
-- Dumped by pg_dump version 9.6.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: divisi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.divisi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.divisi_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: divisi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.divisi (
    id integer DEFAULT nextval('public.divisi_id_seq'::regclass) NOT NULL,
    id_opd integer,
    nama character varying(255)
);


ALTER TABLE public.divisi OWNER TO postgres;

--
-- Name: karyawan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.karyawan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.karyawan_id_seq OWNER TO postgres;

--
-- Name: karyawan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.karyawan (
    id integer DEFAULT nextval('public.karyawan_id_seq'::regclass) NOT NULL,
    id_opd integer,
    id_divisi integer,
    nik character varying(100),
    nama character varying(255),
    tempat_lahir character varying(255),
    tanggal_lahir date,
    jabatan character varying(255),
    alamat text,
    pendidikan character varying(255),
    file character varying(255),
    jenis_kelamin character varying(255)
);


ALTER TABLE public.karyawan OWNER TO postgres;

--
-- Name: opd_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.opd_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.opd_id_seq OWNER TO postgres;

--
-- Name: opd; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.opd (
    id integer DEFAULT nextval('public.opd_id_seq'::regclass) NOT NULL,
    id_jenis_badan integer,
    nama character varying(255)
);


ALTER TABLE public.opd OWNER TO postgres;

--
-- Name: ref_jenis_badan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ref_jenis_badan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ref_jenis_badan_id_seq OWNER TO postgres;

--
-- Name: ref_jenis_badan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ref_jenis_badan (
    id integer DEFAULT nextval('public.ref_jenis_badan_id_seq'::regclass) NOT NULL,
    nama character varying(255)
);


ALTER TABLE public.ref_jenis_badan OWNER TO postgres;

--
-- Name: ref_jenis_opd_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ref_jenis_opd_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ref_jenis_opd_id_seq OWNER TO postgres;

--
-- Name: role_karyawan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_karyawan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_karyawan_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer DEFAULT nextval('public.users_id_seq'::regclass) NOT NULL,
    id_karyawan integer,
    username character varying(255),
    password character varying(255),
    role integer
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Data for Name: divisi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.divisi (id, id_opd, nama) FROM stdin;
1	1	SEKRETARIAT 
0	0	Tidak Ada
10	11	BIDANG PEMBANGUNAN DESA DAN KAWASAN PEDESAAN
11	12	BIDANG PEMBANGUNAN DESA DAN KAWASAN PEDESAAN 
12	21	BAGIAN PROTOKOL
13	22	BAGIAN RUMAH TANGGA
14	23	BAGIAN PRODUKSI DAERAH
15	24	SEKRETARIAT
17	8	BIDANG PENUNJANG
18	8	BIDANG KEPERAWATAN
16	25	BIDANG PENGEMBANGAN KOMPETENSI MANAJERIAL DAN FUNGSI
19	1	testting
20	13	test
21	25	aaa
22	22	aasa
23	1	aaa
\.


--
-- Name: divisi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.divisi_id_seq', 23, true);


--
-- Data for Name: karyawan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.karyawan (id, id_opd, id_divisi, nik, nama, tempat_lahir, tanggal_lahir, jabatan, alamat, pendidikan, file, jenis_kelamin) FROM stdin;
6	22	13	-	Putri Anggraini	Samarinda	1994-06-14	Pramu Pimpinan (Asisten I) Prov Kaltim	Jalan Siti Aisiyah No 55 Rt 06 Rw 03	SMA / 2012	ce2f32da614bb31443cd1bda256c1510.jpg	Perempuan
8	24	1	-	Yhuyun Putriani	Balikpapan	1988-12-04	Tenaga Kontrak	Jalan Anggrek Sirana No. 82 Rt.22 Batu Alam Permai Juanda 1	S1 / 2012	38158449c9cd161f157175afdbc8cd38.jpg	Perempuan
9	25	16	-	Regina Apriyani Saputri	Samarinda	1989-04-29	Pembantu Pengelola Bahan Perencanaan	Jl.Pada Elo no.27 Rt.004 Kel. Baqa Kec. samarinda Seberang	S1 / 2014	a2e19a213658432244ec98c72f116a8f.jpg	Perempuan
7	23	14	-	Dwita vienanda putri	samarinda	1994-07-19	PRAMU TAMU PIMPINAN 	JL. K.H SAMANHUDI GANG. DIRGANTARA NO.32	SMA / 2012	795217686b1515148cd5f5afc0704d94.jpg	Perempuan
5	8	17	-	Isabella Sukma Putri	Samarinda	1898-03-13	Tenaga Pramu Acara	Jl. Jakarta Blok F1 No.9 RT.49 RW.XI Loa Bakung. Samarinda Kalimantan Timur	S1 / 2011	3cf226a32bb58fee40a19018ee21f5bb.jpg	Perempuan
10	1	1	-	test	test	2009-08-08	test	test	test	8123776810125b27a9e782cf15a15927.jpg	Laki-Laki
11	13	20	1	a	a	2020-07-15	1	1	1	2b0636d23fc89e306752da3b1be73d30.jpg	Perempuan
12	1	19	1	aa	aa	2020-06-29	1	1	1	327a2aa6bff1e9889a32a191358166a6.jpg	Laki-Laki
4	1	1	-	Agus Arbainsaaasss	Samarinda	1968-08-17	Sopir	JL.PSuryanata RT 030 Air Putih Samarinda 	-	82628fe8f3c096fb408c8711e247e254.jpg	Laki-Laki
\.


--
-- Name: karyawan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.karyawan_id_seq', 12, true);


--
-- Data for Name: opd; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.opd (id, id_jenis_badan, nama) FROM stdin;
1	2	BADAN KEPEGAWAIAN DAERAH
8	2	DINAS LINGKUNGAN HIDUP
11	1	BADAN PENGELOLAAN KEUANGAN DAN ASET DAERAH
12	1	DINAS PEMBERDAYAAN MASYARAKAT DAN DESA
13	1	SATPOL PP
14	1	SEKRETARIAT DP KORPRI
15	1	SEKRETARIAT DPRD
16	1	RSJD ATMA HUSADA MAHAKAM
17	2	RSUD DR. KANUDJOSO DJATIWIBOWO
18	1	RSUD A.W. SYAHRANIE
19	1	DINAS PU, PENATAAN RUANG & PERUM RAKYAT
0	0	Tidak Ada
21	2	BIRO HUBUNGAN MASYARAKAT
22	2	BIRO UMUM
23	2	BIRO PEREKONOMIAN
24	2	DINAS KOMUNIKASI DAN INFORMATIKA
25	2	BADAN PENGEMBANGAN SUMBER DAYA MANUSIA
26	2	test
\.


--
-- Name: opd_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.opd_id_seq', 26, true);


--
-- Data for Name: ref_jenis_badan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ref_jenis_badan (id, nama) FROM stdin;
2	Lembaga/Biro/Badan
0	Tidak Ada
1	Organisasi Perangkat Daerah
\.


--
-- Name: ref_jenis_badan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_jenis_badan_id_seq', 15, true);


--
-- Name: ref_jenis_opd_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_jenis_opd_id_seq', 5, true);


--
-- Name: role_karyawan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_karyawan_id_seq', 1, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, id_karyawan, username, password, role) FROM stdin;
6	4	agus	agus	2
4	\N	sa	Xs3rc	0
7	4	a	a	1
9	4	agus	agus1	3
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 9, true);


--
-- Name: divisi divisi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.divisi
    ADD CONSTRAINT divisi_pkey PRIMARY KEY (id);


--
-- Name: karyawan karyawan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.karyawan
    ADD CONSTRAINT karyawan_pkey PRIMARY KEY (id);


--
-- Name: opd opd_nama_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opd
    ADD CONSTRAINT opd_nama_key UNIQUE (nama);


--
-- Name: opd opd_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opd
    ADD CONSTRAINT opd_pkey PRIMARY KEY (id);


--
-- Name: ref_jenis_badan ref_jenis_badan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ref_jenis_badan
    ADD CONSTRAINT ref_jenis_badan_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: divisi divisi_opd_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.divisi
    ADD CONSTRAINT divisi_opd_id_fkey FOREIGN KEY (id_opd) REFERENCES public.opd(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: karyawan karyawan_id_divisi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.karyawan
    ADD CONSTRAINT karyawan_id_divisi_fkey FOREIGN KEY (id_divisi) REFERENCES public.divisi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: karyawan karyawan_id_opd_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.karyawan
    ADD CONSTRAINT karyawan_id_opd_fkey FOREIGN KEY (id_opd) REFERENCES public.opd(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: opd opd_id_jenis_badan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opd
    ADD CONSTRAINT opd_id_jenis_badan_fkey FOREIGN KEY (id_jenis_badan) REFERENCES public.ref_jenis_badan(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users users_id_karyawan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_karyawan_fkey FOREIGN KEY (id_karyawan) REFERENCES public.karyawan(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

