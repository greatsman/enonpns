--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.14
-- Dumped by pg_dump version 9.6.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: divisi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.divisi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.divisi_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: divisi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.divisi (
    id integer DEFAULT nextval('public.divisi_id_seq'::regclass) NOT NULL,
    id_opd integer,
    nama character varying(255)
);


ALTER TABLE public.divisi OWNER TO postgres;

--
-- Name: karyawan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.karyawan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.karyawan_id_seq OWNER TO postgres;

--
-- Name: karyawan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.karyawan (
    id integer DEFAULT nextval('public.karyawan_id_seq'::regclass) NOT NULL,
    id_opd integer,
    id_divisi integer,
    nik character varying(100),
    nama character varying(255),
    tempat_lahir character varying(255),
    tanggal_lahir date,
    jabatan character varying(255),
    alamat text,
    pendidikan character varying(255),
    file character varying(255),
    jenis_kelamin character varying(255)
);


ALTER TABLE public.karyawan OWNER TO postgres;

--
-- Name: opd_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.opd_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.opd_id_seq OWNER TO postgres;

--
-- Name: opd; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.opd (
    id integer DEFAULT nextval('public.opd_id_seq'::regclass) NOT NULL,
    id_jenis_badan integer,
    nama character varying(255)
);


ALTER TABLE public.opd OWNER TO postgres;

--
-- Name: ref_jenis_badan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ref_jenis_badan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ref_jenis_badan_id_seq OWNER TO postgres;

--
-- Name: ref_jenis_badan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ref_jenis_badan (
    id integer DEFAULT nextval('public.ref_jenis_badan_id_seq'::regclass) NOT NULL,
    nama character varying(255)
);


ALTER TABLE public.ref_jenis_badan OWNER TO postgres;

--
-- Name: ref_jenis_opd_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ref_jenis_opd_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ref_jenis_opd_id_seq OWNER TO postgres;

--
-- Name: role_karyawan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_karyawan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_karyawan_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer DEFAULT nextval('public.users_id_seq'::regclass) NOT NULL,
    id_karyawan integer,
    username character varying(255),
    password character varying(255),
    role integer
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Data for Name: divisi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.divisi (id, id_opd, nama) FROM stdin;
1	1	SEKRETARIAT 
0	0	Tidak Ada
10	11	BIDANG PEMBANGUNAN DESA DAN KAWASAN PEDESAAN
11	12	BIDANG PEMBANGUNAN DESA DAN KAWASAN PEDESAAN 
\.


--
-- Name: divisi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.divisi_id_seq', 11, true);


--
-- Data for Name: karyawan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.karyawan (id, id_opd, id_divisi, nik, nama, tempat_lahir, tanggal_lahir, jabatan, alamat, pendidikan, file, jenis_kelamin) FROM stdin;
4	1	1	-	Agus Arbain	Samarinda	1968-08-17	Sopir	JL.PSuryanata RT 030 Air Putih Samarinda 	-	fad3e3f6b15729c9e9d113bf65fb720c.jpg	Laki-Laki
\.


--
-- Name: karyawan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.karyawan_id_seq', 4, true);


--
-- Data for Name: opd; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.opd (id, id_jenis_badan, nama) FROM stdin;
1	2	BADAN KEPEGAWAIAN DAERAH
8	2	DINAS LINGKUNGAN HIDUP
11	1	BADAN PENGELOLAAN KEUANGAN DAN ASET DAERAH
12	1	DINAS PEMBERDAYAAN MASYARAKAT DAN DESA
13	1	SATPOL PP
14	1	SEKRETARIAT DP KORPRI
15	1	SEKRETARIAT DPRD
16	1	RSJD ATMA HUSADA MAHAKAM
17	2	RSUD DR. KANUDJOSO DJATIWIBOWO
18	1	RSUD A.W. SYAHRANIE
19	1	DINAS PU, PENATAAN RUANG & PERUM RAKYAT
0	0	Tidak Ada
\.


--
-- Name: opd_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.opd_id_seq', 20, true);


--
-- Data for Name: ref_jenis_badan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ref_jenis_badan (id, nama) FROM stdin;
2	Lembaga/Biro/Badan
0	Tidak Ada
1	Organisasi Perangkat Daerah
\.


--
-- Name: ref_jenis_badan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_jenis_badan_id_seq', 15, true);


--
-- Name: ref_jenis_opd_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_jenis_opd_id_seq', 5, true);


--
-- Name: role_karyawan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_karyawan_id_seq', 1, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, id_karyawan, username, password, role) FROM stdin;
6	4	agus	agus	2
4	\N	sa	Xs3rc	0
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 6, true);


--
-- Name: divisi divisi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.divisi
    ADD CONSTRAINT divisi_pkey PRIMARY KEY (id);


--
-- Name: karyawan karyawan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.karyawan
    ADD CONSTRAINT karyawan_pkey PRIMARY KEY (id);


--
-- Name: opd opd_nama_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opd
    ADD CONSTRAINT opd_nama_key UNIQUE (nama);


--
-- Name: opd opd_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opd
    ADD CONSTRAINT opd_pkey PRIMARY KEY (id);


--
-- Name: ref_jenis_badan ref_jenis_badan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ref_jenis_badan
    ADD CONSTRAINT ref_jenis_badan_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: divisi divisi_opd_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.divisi
    ADD CONSTRAINT divisi_opd_id_fkey FOREIGN KEY (id_opd) REFERENCES public.opd(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: karyawan karyawan_id_divisi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.karyawan
    ADD CONSTRAINT karyawan_id_divisi_fkey FOREIGN KEY (id_divisi) REFERENCES public.divisi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: karyawan karyawan_id_opd_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.karyawan
    ADD CONSTRAINT karyawan_id_opd_fkey FOREIGN KEY (id_opd) REFERENCES public.opd(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: opd opd_id_jenis_badan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opd
    ADD CONSTRAINT opd_id_jenis_badan_fkey FOREIGN KEY (id_jenis_badan) REFERENCES public.ref_jenis_badan(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users users_id_karyawan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_karyawan_fkey FOREIGN KEY (id_karyawan) REFERENCES public.karyawan(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

