--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.14
-- Dumped by pg_dump version 9.6.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: divisi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.divisi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.divisi_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: divisi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.divisi (
    id integer DEFAULT nextval('public.divisi_id_seq'::regclass) NOT NULL,
    id_opd integer,
    nama character varying(255)
);


ALTER TABLE public.divisi OWNER TO postgres;

--
-- Name: opd_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.opd_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.opd_id_seq OWNER TO postgres;

--
-- Name: opd; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.opd (
    id integer DEFAULT nextval('public.opd_id_seq'::regclass) NOT NULL,
    id_jenis_badan integer,
    nama character varying(255)
);


ALTER TABLE public.opd OWNER TO postgres;

--
-- Name: ref_jenis_badan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ref_jenis_badan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ref_jenis_badan_id_seq OWNER TO postgres;

--
-- Name: ref_jenis_badan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ref_jenis_badan (
    id integer DEFAULT nextval('public.ref_jenis_badan_id_seq'::regclass) NOT NULL,
    nama character varying(255),
    level character varying(255)
);


ALTER TABLE public.ref_jenis_badan OWNER TO postgres;

--
-- Name: ref_jenis_opd_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ref_jenis_opd_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ref_jenis_opd_id_seq OWNER TO postgres;

--
-- Name: role_karyawan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_karyawan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_karyawan_id_seq OWNER TO postgres;

--
-- Name: role_karyawan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role_karyawan (
    id integer DEFAULT nextval('public.role_karyawan_id_seq'::regclass) NOT NULL,
    id_opd integer,
    id_divisi integer,
    id_users integer
);


ALTER TABLE public.role_karyawan OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer DEFAULT nextval('public.users_id_seq'::regclass) NOT NULL,
    nik character varying(100),
    nama character varying(255),
    tempat_lahir character varying(255),
    tanggal_lahir date,
    jabatan character varying(255),
    alamat character varying(255),
    pendidikan character varying(255),
    username character varying(255),
    password character varying(255),
    role integer
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Data for Name: divisi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.divisi (id, id_opd, nama) FROM stdin;
1	1	SEKRETARIAT 
2	2	BIDANG ANGGARAN 
3	3	BAGIAN RUMAH TANGGA 
\.


--
-- Name: divisi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.divisi_id_seq', 4, true);


--
-- Data for Name: opd; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.opd (id, id_jenis_badan, nama) FROM stdin;
1	2	BADAN KEPEGAWAIAN DAERAH
2	3	BADAN PENGELOLAAN KEUANGAN DAN ASET DAERAH
3	3	BIRO UMUM
8	2	DINAS LINGKUNGAN HIDUP
\.


--
-- Name: opd_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.opd_id_seq', 10, true);


--
-- Data for Name: ref_jenis_badan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ref_jenis_badan (id, nama, level) FROM stdin;
1	Super Admin	0
2	Organisasi Perangkat Daerah	1
3	Lembaga/Biro/Badan	1
4	Operator	2
5	Pegawai	3
\.


--
-- Name: ref_jenis_badan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_jenis_badan_id_seq', 14, true);


--
-- Name: ref_jenis_opd_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_jenis_opd_id_seq', 5, true);


--
-- Data for Name: role_karyawan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role_karyawan (id, id_opd, id_divisi, id_users) FROM stdin;
1	1	1	1
\.


--
-- Name: role_karyawan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_karyawan_id_seq', 1, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, nik, nama, tempat_lahir, tanggal_lahir, jabatan, alamat, pendidikan, username, password, role) FROM stdin;
4	0000000	sa	\N	\N	\N	\N		sa	sa	1
1	0000000	Agus Nordiansyah	Samboja	1979-01-04	Caraka	JL.Dr.Soetomo GG 2 No 4	SMA / 1998	312100008	312100008	5
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 4, true);


--
-- Name: divisi divisi_nama_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.divisi
    ADD CONSTRAINT divisi_nama_key UNIQUE (nama);


--
-- Name: divisi divisi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.divisi
    ADD CONSTRAINT divisi_pkey PRIMARY KEY (id);


--
-- Name: opd opd_nama_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opd
    ADD CONSTRAINT opd_nama_key UNIQUE (nama);


--
-- Name: opd opd_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opd
    ADD CONSTRAINT opd_pkey PRIMARY KEY (id);


--
-- Name: ref_jenis_badan ref_jenis_badan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ref_jenis_badan
    ADD CONSTRAINT ref_jenis_badan_pkey PRIMARY KEY (id);


--
-- Name: role_karyawan role_karyawan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_karyawan
    ADD CONSTRAINT role_karyawan_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- Name: divisi divisi_opd_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.divisi
    ADD CONSTRAINT divisi_opd_id_fkey FOREIGN KEY (id_opd) REFERENCES public.opd(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: opd opd_id_jenis_badan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opd
    ADD CONSTRAINT opd_id_jenis_badan_fkey FOREIGN KEY (id_jenis_badan) REFERENCES public.ref_jenis_badan(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: role_karyawan role_karyawan_divisi_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_karyawan
    ADD CONSTRAINT role_karyawan_divisi_id_fkey FOREIGN KEY (id_divisi) REFERENCES public.divisi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: role_karyawan role_karyawan_opd_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_karyawan
    ADD CONSTRAINT role_karyawan_opd_id_fkey FOREIGN KEY (id_opd) REFERENCES public.opd(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: role_karyawan role_karyawan_users_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_karyawan
    ADD CONSTRAINT role_karyawan_users_id_fkey FOREIGN KEY (id_users) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users users_role_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_role_fkey FOREIGN KEY (role) REFERENCES public.ref_jenis_badan(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

