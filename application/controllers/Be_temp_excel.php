<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_temp_excel extends Secure {

    public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('login') == FALSE){
            redirect('login');
        }

        $this->load->model('akses_model', 'akses', true);
        $this->load->helper('download');   
    }

    public function index()
    {
        $data = array(
            // 'tahuns'        => $tahuns,
            'arrAccess'     => $this->akses->getArrAccess(),
            // 'styleExtra'    => $this->load->view('profil_desa/style_profil_desa', '', true),
            'scriptExtra'   => $this->load->view('temp_excel/script_temp_excel', '', true),
            

        );
        // var_dump($data['arrAccess']); exit();
        $this->_render('temp_excel/ambil', $data);
    }

    public function ambil(){
        $berkas = $this->uri->segment(3);
        $file_name = $berkas.'.xls';
        // if ($berkas == 'profil_desa'){
        //     $file_name = 'profil_desa.xls';
        // }
        // if ($berkas == 'dokumentasi'){
        //     $file_name = 'dokumentasi.xls';
        // }

        $file_name = FCPATH.'excel_tmp/'.$file_name;
        force_download($file_name, null);

        redirect('temp_excel');
    }
}