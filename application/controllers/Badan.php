<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Badan extends Secure {

    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('login') == FALSE){
            redirect('Login');
        }
        $this->load->model('Badan_model','badan',true);

    }

    public function index()
    {
        $data = array(
            'styleExtra'        => $this->load->view('badan/style', '', true),
            'scriptExtra'       => $this->load->view('badan/script', '', true)
        );

        $this->_render('badan/list', $data);
    }

    private function fvalidation(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        
        $this->form_validation->set_message('required', '%s masih kosong, silahkan isi');
        $this->form_validation->set_error_delimiters('<span class="help-block">&nbsp;</span>');

    }

    function datatables(){
        $data = $this->badan->get_all();
        echo json_encode($data);
    }

        // simpan
    public function add()
    {
        $this->fvalidation();
        if ($this->form_validation->run() == TRUE) {

            $data = array(
                'nama'              => $this->input->post('nama')
            );

            $pesan = "Jenis Badan Berhasil Ditambahkan";
            $this->session->set_flashdata('pesan_sks', $pesan);
            $status = $this->badan->save($data);
            redirect('Badan');

        }else{
            $data = array(
                'styleExtra'    => $this->load->view('badan/style', '', true),
                'scriptExtra'   => $this->load->view('badan/script', '', true),

            );
            $this->_render('badan/add',$data);
        }


    }

    public function edit()
    {
        $id = $this->uri->segment(3);
        $row = $this->badan->get($id);
        $this->fvalidation();
        $data = array(
            'styleExtra'        => $this->load->view('badan/style', '', true),
            'scriptExtra'       => $this->load->view('badan/script', '', true),
            'dt'                => $row,      
        );
        $this->_render('badan/edit',$data);
    }


    public function update()
    {
        $id = $this->uri->segment(3);
        $this->fvalidation();
        if ($this->form_validation->run() == TRUE) {

            $data = array(
                'nama'              => $this->input->post('nama')
            );

            $pesan = "Jenis Badan Berhasil Ditambahkan";
            $this->session->set_flashdata('pesan_sks', $pesan);
            $status = $this->badan->update($id, $data);
            redirect('Badan');

        }else{
            $data = array(
                'styleExtra'    => $this->load->view('badan/style', '', true),
                'scriptExtra'   => $this->load->view('badan/script', '', true),

            );
            $this->_render('badan/edit',$data);
        }


    }

    public function delete() {
        $id=$this->input->post('id');
        $data=$this->badan->delete($id);
        echo json_encode($data);
    }

}
