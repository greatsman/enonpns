<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Secure {

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('login') == FALSE){
            $this->session->set_flashdata('msg_warning', 'Session telah kadaluarsa, silahkan login ulang.');
            redirect('Login');
        }
        $this->load->model('Pegawai_model','pegawai',true);
        $this->load->model('Users_model','users',true);
    }

    public function index()
    {
        $data = array(
            'styleExtra'        => $this->load->view('users/style', '', true),
            'scriptExtra'       => $this->load->view('users/script', '', true)
        );

        $this->_render('users/list', $data);
    }

    private function fvalidation() {
        $this->load->library('form_validation');
        if($this->input->post('username') != $original_value) {
           $is_unique =  '|is_unique[users.username]';
        } else {
           $is_unique =  '';
        }
        $this->form_validation->set_rules('id_karyawan', 'Karyawan', 'required|trim');
        $this->form_validation->set_rules('username', 'username', 'required|trim'.$is_unique);
        $this->form_validation->set_rules('password', 'password', 'required|trim');

        $this->form_validation->set_message('required', '%s masih kosong, silahkan isi');
        $this->form_validation->set_message('is_unique', '%s sudah digunakan');
        $this->form_validation->set_error_delimiters('<span class="help-block">&nbsp;</span>');
    }

    private function editvalidation() {
        $this->load->library('form_validation');
        if($this->input->post('username') != $original_value) {
           $is_unique =  '|is_unique[users.username]';
        } else {
           $is_unique =  '';
        }
        $this->form_validation->set_rules('id_karyawan', 'Karyawan', 'required|trim');
        $this->form_validation->set_rules('username', 'username', 'required|trim');
        $this->form_validation->set_rules('password', 'password', 'required|trim');

        $this->form_validation->set_message('required', '%s masih kosong, silahkan isi');
        $this->form_validation->set_message('is_unique', '%s sudah digunakan, input %s lain');
        $this->form_validation->set_error_delimiters('<span class="help-block">&nbsp;</span>');
    }

    function datatables(){
        $data = $this->users->get_all();
        echo json_encode($data);
    }


    public function add()
    {
        $this->fvalidation();
        if ($this->form_validation->run() == TRUE) {

            $data = array(
                'id_karyawan'       => $this->input->post('id_karyawan'),
                'username'          => $this->input->post('username'),
                'password'          => $this->input->post('password'),
                'role'              => $this->input->post('role'),
            );

            $pesan = "Users Berhasil Ditambahkan";
            $this->session->set_flashdata('pesan_sks', $pesan);
            $status = $this->users->save($data);
            redirect('Users');

        }else{

            $data = array(
                'pegawai'       => $this->pegawai->get_all(),
                'styleExtra'    => $this->load->view('users/style', '', true),
                'scriptExtra'   => $this->load->view('users/script', '', true),

            );
            $this->_render('users/add',$data);
        }
    }

    public function edit(){
        $id = $this->uri->segment(3);
        $row = $this->users->get($id);

        $this->editvalidation();
        $data = array(
            'pegawai'           => $this->pegawai->get_all(),
            'styleExtra'        => $this->load->view('opd/style', '', true),
            'scriptExtra'       => $this->load->view('opd/script', '', true),
            'dt'                => $row,      
        );
        $this->_render('users/edit',$data);
    }


    public function update()
    {
        $id = $this->uri->segment(3);

        $this->editvalidation();
        if ($this->form_validation->run() == TRUE) {
            $data = array(
                'id_karyawan'       => $this->input->post('id_karyawan'),
                'username'          => $this->input->post('username'),
                'password'          => $this->input->post('password'),
                'role'              => $this->input->post('role'),
            );
            $status = $this->users->update($id, $data);

            $pesan = "Users Berhasil Diupdate";
            $this->session->set_flashdata('pesan_sks', $pesan);
            redirect('Users');

        }else{
            $data = array(
                'pegawai'       => $this->pegawai->get_all(),
                'styleExtra'    => $this->load->view('pegawai/style', '', true),
                'scriptExtra'   => $this->load->view('pegawai/script', '', true),

            );
            $this->_render('users/add',$data);
        }

    }

    public function delete() {
        $id=$this->input->post('id');
        $data=$this->users->delete($id);
        echo json_encode($data);
    }


}
