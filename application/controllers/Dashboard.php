<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Secure {

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('login') == FALSE){
            redirect('Login');
        }
    }

    public function index()
    {
        $this->_render('dashboard/list', $data);
    }
}
