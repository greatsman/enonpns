<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biodata extends Secure {

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('login') == FALSE){
            $this->session->set_flashdata('msg_warning', 'Session telah kadaluarsa, silahkan login ulang.');
            redirect('Login');
        }
        $this->load->model('Pegawai_model','pegawai',true);
        $this->load->model('Opd_model','opd',true);
        $this->load->model('Divisi_model','divisi',true);
        $this->load->library('upload');
    }

    public function index()
    {
        $id = $this->session->userdata('id_karyawan');
        $row = $this->pegawai->get_pegawai_detail($id);

        $data = array(
            'dt'                => $row,      
        );
        $this->_render('biodata/index',$data);
    }

    private function fvalidation() {
        $this->load->library('form_validation');
        if($this->input->post('nik') != $original_value) {
           $is_unique =  '|is_unique[karyawan.nik]';
        } else {
           $is_unique =  '';
        }

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('nik', 'Nik', 'required|trim');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('pendidikan', 'Pendidikan', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');


        $this->form_validation->set_message('required', '%s masih kosong, silahkan isi');
        $this->form_validation->set_message('is_unique', '%s sudah digunakan, input %s lain');
        $this->form_validation->set_error_delimiters('<span class="help-block">&nbsp;</span>');
    }

    public function edit(){
        $id = $this->session->userdata('id_karyawan');
        $row = $this->pegawai->get_pegawai_detail($id);
        $this->fvalidation();

        $data = array(
            'styleExtra'        => $this->load->view('biodata/style', '', true),
            'scriptExtra'       => $this->load->view('biodata/script', '', true),
            'dt'                => $row,      
        );
        $this->_render('biodata/edit',$data);
    }

    public function update()
    {
        $id = $this->session->userdata('id_karyawan');

        $this->fvalidation();
        if ($this->form_validation->run() == TRUE) {
            $data = array(
                'nama'              => $this->input->post('nama'),
                'nik'               => $this->input->post('nik'),
                'tempat_lahir'      => $this->input->post('tempat_lahir'),
                'tanggal_lahir'     => $this->input->post('tanggal_lahir'),
                'jabatan'           => $this->input->post('jabatan'),
                'alamat'            => $this->input->post('alamat'),
                'pendidikan'        => $this->input->post('pendidikan'),
                'jenis_kelamin'     => $this->input->post('jenis_kelamin')
            );
            $status = $this->pegawai->update($id, $data);



            if (!empty($_FILES['file_upload']['name'])) {
                $config['upload_path'] = './uploads/foto/pegawai/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; 
                $config['encrypt_name'] = TRUE;

                $this->upload->initialize($config);
                if (!empty($_FILES['file_upload']['name'])) {

                    if ($this->upload->do_upload('file_upload')) {
                        $gbr = $this->upload->data();
                        //Compress Image
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = './uploads/foto/pegawai/' . $gbr['file_name'];
                        $config['create_thumb'] = FALSE;
                        $config['maintain_ratio'] = FALSE;
                        $config['new_image'] = './uploads/foto/pegawai/' . $gbr['file_name'];
                        $this->load->library('image_lib', $config);
                        $this->image_lib->resize();

                        $gambar = $gbr['file_name'];
                    } else {
                        $gambar = '';
                    }
                } else {
                    $gambar = '';
                }

                $Path = FCPATH.'uploads\foto\pegawai\\'.$this->input->post('file_lama');
                unlink($Path);


                $data3 = array(
                    'file'          => $gambar,
                );

                $status1 = $this->pegawai->update($id, $data3);
            }


            $pesan = "Pegawai Berhasil Diupdate";
            $this->session->set_flashdata('pesan_sks', $pesan);
            redirect('Biodata');

        }

    }

}
