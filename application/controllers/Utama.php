<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utama extends Secure {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Dashboard_model','dashboard',true);
        $this->load->model('Opd_model','opd',true);
    }

    public function index()
    {
        // var_dump($this->dashboard->get_sum_lembaga());exit();
        $data = array(
            'lembaga'       => $this->dashboard->get_all_lembaga(),
            'sum_lembaga'   => $this->dashboard->get_sum_lembaga(),
            'opd'           => $this->dashboard->get_all_opd(),
            'all_opd'       => $this->dashboard->get_all_opd_ref(),
            'sum_opd'       => $this->dashboard->get_sum_opd(),
            'jk'            => $this->dashboard->get_all_karyawan_jk(),
            'sum_pegawai'   => $this->dashboard->get_all_karyawan(),
        );
        $this->_render_front('front_end/dashboard', $data);
    }
}
