<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Be_reset_password extends Secure {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('pengguna_model','pengguna',true);

        // if($this->session->userdata('login') == FALSE){
        //     redirect('login');
        // }
    }

    public function index()
    {

    }

    public function edit(){
        $pid = $this->uri->segment(3);

        $data = array(
            // 'styleExtra'    => $this->load->view('pengguna/style_reset_password', '', true),
            'scriptExtra'   => $this->load->view('reset_password/script_reset_password', '', true),
            'pid'           => $pid
        );
        // $this->_render('reset_password/edit', $data);
        // var_dump($pid); exit();
        $this->load->view('reset_password/edit', $data);
    }
    public function update() {
        $pid = $this->input->post('pidd');
        $new_password = $this->input->post('password');
        if ($this->pengguna->setNewPassword($pid, $new_password)) {
            $pesan = "User Berhasil Diubah";
            $this->session->set_flashdata('pesan_sks', $pesan);
            redirect('login');
        } else {
            $pesan = "User, Data Gagal Diubah";
            $this->session->set_flashdata('pesan_ggl', $pesan);
            redirect('login');
        }
    }

}
