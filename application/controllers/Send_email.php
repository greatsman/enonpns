<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Send_email extends CI_Controller {

    /**
     * Kirim email dengan SMTP Gmail.
     *
     */

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('pengguna_model','pengguna',true);

        $this->load->helper('string');

        // if($this->session->userdata('login') == FALSE){
        //     redirect('login');
        // }
    }
    
    public function index()
    {
        // $e_address = $this->uri->segment(2);
        // echo $e_address;
    }

    private function generate_pid(){
        return random_string('alnum', 16);
    }

    private function mark_reset($email){
        $pid = $this->generate_pid();
        $this->pengguna->setResetPid($email, $pid);
        return $pid;
    }

    public function do_send(){
        // $e_address = $this->uri->segment(3);
        $e_address = md5($this->input->post('email_reset'));

        // var_dump($e_address); exit();
      // Konfigurasi email
        $config = [
               'mailtype'  => 'html',
               'charset'   => 'utf-8',
               'protocol'  => 'smtp',
               'smtp_host' => 'ssl://smtp.gmail.com',
               'smtp_user' => 'bbws.surakarta.dev@gmail.com',    // Ganti dengan email gmail kamu
               'smtp_pass' => 'rahasiasekali3',      // Password gmail kamu
               'smtp_port' => 465,
               'crlf'      => "\r\n",
               'newline'   => "\r\n"
           ];

        // Load library email dan konfigurasinya
        $this->load->library('email', $config);

        // Email dan nama pengirim
        $this->email->from('bbws.surakarta.dev@gmail.com', 'Administrator');

        // Email penerima
        $e_address_r = $this->pengguna->getEmailByMd5($e_address);
        $this->email->to($e_address_r); // Ganti dengan email tujuan kamu

        // var_dump($e_address_r); exit();

        // Lampiran email, isi dengan url/path file
        // $this->email->attach('https://masrud.com/content/images/20181215150137-codeigniter-smtp-gmail.png');

        // Subject email

        $this->email->subject('Konfirmasi Reset Password');

        // Isi email
        $pid = $this->mark_reset($e_address_r);
        $this->email->message('Silahkan menuju link ini untuk reset password : <a href="'.base_url().'reset_password/edit/'.$pid.'">Reset Password</a>');

        // Tampilkan pesan sukses atau error
        if ($this->email->send()) {
            $this->session->set_flashdata('pesan_sks', $pesan);

            redirect('Login');
        } else {
            $this->session->set_flashdata('pesan_ggl', $pesan);

            // redirect('Login');
        }
    }
}