<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Login_model', '', TRUE);
    }

    public function index()
    {
        // cek session
        if ($this->session->userdata('login')) {
            if ($this->session->userdata('role') == 3) {
                redirect('Biodata');
            }else{
                redirect('Dashboard');
            }
        } else {
            // var_dump($tahapans);
            $data = array(
                'content' => 'Admin'
            );
            $this->load->view('login', $data);
        }
    }


    public function opd()
    {
        // cek session
        if ($this->session->userdata('login')) {
            redirect('Dashboard');
        } else {
            // var_dump($tahapans);
            $data = array(
                'content' => 'OPD'
            );
            $this->load->view('login', $data);
        }
    }


    public function pegawai()
    {
        // cek session
        if ($this->session->userdata('login')) {
            redirect('Dashboard');
        } else {
            // var_dump($tahapans);
            $data = array(
                'content' => 'Pegawai'
            );
            $this->load->view('login', $data);
        }
    }



    public function login()
    {

        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $row = $this->Login_model->login($username, $password)->row();

        if ($row) {
            // login berhasil
            $this->_daftarkan_session($row);
            redirect('Back_password');
        } else {
            // login gagal
            $pesan = "Maaf, Username atau Password Tidak Cocok";
            $this->session->set_flashdata('pesan_ggl', $pesan);
            $this->index();
        }
    }

    public function auth()
    {

        if ($this->Login_model->save($this->input->post())) {
            $pesan = "User Baru Berhasil Ditambahkan";
            $this->session->set_flashdata('pesan_sks', $pesan);
            redirect('Login');
        } else {
            $pesan = "User, Data Gagal Ditambahkan";
            $this->session->set_flashdata('pesan_ggl', $pesan);
            redirect('Login');
        }
    }

    public function _daftarkan_session($row)
    {
        // 1. Daftarkan Session
        if ($row->role == 0) {
            $sess = array(
                'id'        => $row->id,
                'username'  => $row->username,
                'role'      => $row->role,
                'login'     => TRUE
            );
        }else{
            $get_id = $this->Login_model->get_id_baru($row->id_karyawan);
            $sess = array(
                'id'            => $row->id,
                'username'      => $row->username,
                'role'          => $row->role,
                'id_divisi'     => $get_id->id_divisi,
                'id_opd'        => $get_id->id_opd,
                'id_badan'      => $get_id->id_badan,
                'id_karyawan'   => $row->id_karyawan,
                'login'         => TRUE,
            );
        }
        $this->session->set_userdata($sess);
        $this->index();
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('Login');
    }
}
