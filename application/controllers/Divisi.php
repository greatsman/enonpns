<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Divisi extends Secure {

    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('login') == FALSE){
            redirect('Login');
        }

        $this->load->model('Opd_model','opd',true);
        $this->load->model('Divisi_model','divisi',true);

    }

    public function index()
    {
        $data = array(
            'styleExtra'        => $this->load->view('divisi/style', '', true),
            'scriptExtra'       => $this->load->view('divisi/script', '', true)
        );

        $this->_render('divisi/list', $data);
    }

    private function fvalidation(){
        $this->load->library('form_validation');
        if($this->input->post('nama') != $original_value) {
           $is_unique =  '|is_unique[divisi.nama]';
        } else {
           $is_unique =  '';
        }

        if($this->input->post('id_opd') != $original_value) {
           $is_unique2 =  '|is_unique[divisi.id_opd]';
        } else {
           $is_unique2 =  '';
        }

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('id_opd', 'opd', 'required');

        $this->form_validation->set_message('required', '%s masih kosong, silahkan isi');
        $this->form_validation->set_message('is_unique2', '%s sudah digunakan, input OPD lain');
        $this->form_validation->set_error_delimiters('<span class="help-block">&nbsp;</span>');
    }

    function datatables(){
        $data = $this->divisi->get_all();
        echo json_encode($data);
    }


    public function add()
    {
        $this->fvalidation();
        if ($this->form_validation->run() == TRUE) {

            $data = array(
                'nama'      => $this->input->post('nama'),
                'id_opd'    => $this->input->post('id_opd')
            );

            $pesan = "Divisi Berhasil Ditambahkan";
            $this->session->set_flashdata('pesan_sks', $pesan);
            $status = $this->divisi->save($data);
            redirect('Divisi');

        }else{
            $data = array(
                'opd'           => $this->opd->get_all(),
                'styleExtra'    => $this->load->view('divisi/style', '', true),
                'scriptExtra'   => $this->load->view('divisi/script', '', true),

            );
            $this->_render('divisi/add',$data);
        }


    }

    public function edit(){
        $id     = $this->uri->segment(3);
        $row    = $this->divisi->get($id);
        $this->fvalidation();
        $data = array(
            'opd'               => $this->opd->get_all(),
            'styleExtra'        => $this->load->view('divisi/style', '', true),
            'scriptExtra'       => $this->load->view('divisi/script', '', true),
            'dt'                => $row,      
        );
        $this->_render('divisi/edit',$data);
    }


    public function update()
    {
        $id = $this->uri->segment(3);

        $this->fvalidation();
        if ($this->form_validation->run() == TRUE) {

            $data = array(
                'nama'      => $this->input->post('nama'),
                'id_opd'    => $this->input->post('id_opd')
            );

            $pesan = "Divisi Berhasil Diupdate";
            $this->session->set_flashdata('pesan_sks', $pesan);
            $status = $this->divisi->update($id, $data);
            redirect('Divisi');

        }else{
            $data = array(
                'opd'           => $this->opd->get_all(),
                'styleExtra'    => $this->load->view('divisi/style', '', true),
                'scriptExtra'   => $this->load->view('divisi/script', '', true),

            );
            $this->_render('divisi/add',$data);
        }


    }

    public function delete() {
        $id=$this->input->post('id');
        $data=$this->divisi->delete($id);
        echo json_encode($data);
    }




}
