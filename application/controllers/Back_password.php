<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Back_password extends Secure {

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('password_model','password',true);
        // if($this->session->userdata('login') == FALSE){
        //     redirect('login');
        // }
    }

    public function index()
    {
        $data = array(
            'styleExtra'    => $this->load->view('password/style_password', '', true),
            'scriptExtra'   => $this->load->view('password/script_password', '', true),
        );
        $this->_render('password/list',$data);
    }

}
