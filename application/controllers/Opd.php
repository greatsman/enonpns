<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Opd extends Secure
{

    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('login') == FALSE) {
            redirect('Login');
        }
        $this->load->model('Opd_model', 'opd', true);
        $this->load->model('Badan_model', 'badan', true);
    }

    public function index()
    {
        $data = array(
            'styleExtra'        => $this->load->view('opd/style', '', true),
            'scriptExtra'       => $this->load->view('opd/script', '', true)
        );

        $this->_render('opd/list', $data);
    }

    private function fvalidation()
    {
        $this->load->library('form_validation');
        if ($this->input->post('nama') != $original_value) {
            $is_unique =  '|is_unique[opd.nama]';
        } else {
            $is_unique =  '';
        }

        $this->form_validation->set_rules('nama', ' Nama OPD', 'required|trim' . $is_unique);
        $this->form_validation->set_rules('id_jenis_badan', 'id_jenis_badan', 'required');

        $this->form_validation->set_message('required', '%s masih kosong, silahkan isi');
        $this->form_validation->set_message('is_unique', '%s sudah digunakan, input nama OPD lain');
        $this->form_validation->set_error_delimiters('<span class="help-block">&nbsp;</span>');
    }


    private function editvalidation()
    {
        $this->load->library('form_validation');
        if ($this->input->post('nama') != $original_value) {
            $is_unique =  '|is_unique[opd.nama]';
        } else {
            $is_unique =  '';
        }

        $this->form_validation->set_rules('nama', ' Nama OPD', 'required|trim');
        $this->form_validation->set_rules('id_jenis_badan', 'id_jenis_badan', 'required');

        $this->form_validation->set_message('required', '%s masih kosong, silahkan isi');
        $this->form_validation->set_message('is_unique', '%s sudah digunakan, input nama OPD lain');
        $this->form_validation->set_error_delimiters('<span class="help-block">&nbsp;</span>');
    }

    function datatables()
    {
        $data = $this->opd->get_all();
        echo json_encode($data);
    }


    public function add()
    {
        $this->fvalidation();
        if ($this->form_validation->run() == TRUE) {

            $data = array(
                'nama'              => $this->input->post('nama'),
                'id_jenis_badan'    => $this->input->post('id_jenis_badan')
            );

            $pesan = "OPD Berhasil Ditambahkan";
            $this->session->set_flashdata('pesan_sks', $pesan);
            $status = $this->opd->save($data);
            redirect('Opd');
        } else {
            $data = array(
                'badan'         => $this->badan->get_all(),
                'styleExtra'    => $this->load->view('opd/style', '', true),
                'scriptExtra'   => $this->load->view('opd/script', '', true),

            );
            $this->_render('opd/add', $data);
        }
    }

    public function edit()
    {
        $id = $this->uri->segment(3);
        $row = $this->opd->get($id);
        $this->fvalidation();

        $data = array(
            'badan'             => $this->badan->get_all(),
            'styleExtra'        => $this->load->view('opd/style', '', true),
            'scriptExtra'       => $this->load->view('opd/script', '', true),
            'dt'                => $row,
        );
        $this->_render('opd/edit', $data);
    }


    public function update()
    {
        $id = $this->uri->segment(3);

        $this->editvalidation();
        if ($this->form_validation->run() == TRUE) {

            $data = array(
                'nama'              => $this->input->post('nama'),
                'id_jenis_badan'    => $this->input->post('id_jenis_badan')
            );

            $pesan = "OPD Berhasil Diupdate";
            $this->session->set_flashdata('pesan_sks', $pesan);
            $status = $this->opd->update($id, $data);
            redirect('Opd');
        } else {
            $data = array(
                'badan'         => $this->badan->get_all(),
                'styleExtra'    => $this->load->view('opd/style', '', true),
                'scriptExtra'   => $this->load->view('opd/script', '', true),

            );
            $this->_render('opd/add', $data);
        }
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $data = $this->opd->delete($id);
        echo json_encode($data);
    }
}
