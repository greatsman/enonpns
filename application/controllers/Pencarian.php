<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pencarian extends Secure {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Dashboard_model','dashboard',true);
    }

    public function index()
    {
    	$data = array(
    		'opd'     => $this->dashboard->get_all_opd_ref(),
    	);
        $this->_render_front('front_end/pencarian', $data);
    }

    public function cari()
    {
    	$kunci = $this->input->post('nama');
    	$opd = $this->input->post('id_opd');
    	if ($kunci == null || $opd == null) {
    		redirect('Pencarian');
    	}
	    $result = $this->dashboard->get_pegawai_search($kunci, $opd);
    	if ($kunci == FALSE) {
	    	$data = array(
	    		'opd'     => $this->dashboard->get_all_opd_ref(),
	    	);
        	$this->_render_front('front_end/pencarian', $data);
    	}else{
    		$data = array(
	    		'opd'     		=> $this->dashboard->get_all_opd_ref(),
	    		'pegawai' 		=> $result,
	    		'kunci' 		=> $kunci,
	    	);
        	$this->_render_front('front_end/result_pencarian', $data);
    	}
    }

    public function detail($id)
	{
		$data = [
            'data' => $this->dashboard->get_pegawai_detail($id),
        ];

		$this->load->view('front_end/detail_pencarian', $data);
	}
}
