<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Back_user extends Secure {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model','user',true);
        if($this->session->userdata('login') == FALSE){
            redirect('login');
        }
        if($this->session->userdata('id') != 1){
            redirect('login');
        }
    }

    public function index()
    {
        $data = array(
            'styleExtra'    => $this->load->view('user/style_user', '', true),
            'scriptExtra'   => $this->load->view('user/script_user', '', true),

        );
        $this->_render('user/list',$data);
    }

    public function add() {
        $data = array(
            'styleExtra'    => $this->load->view('user/style_user', '', true),
            'scriptExtra'   => $this->load->view('user/script_user', '', true),

        );
        $this->_render('user/add',$data);
    }

    public function save() {
        
        if ($this->user->save($this->input->post())) {
            $pesan = "User Berhasil Ditambahkan";
            $this->session->set_flashdata('pesan_sks', $pesan);
            redirect('back_user');
        } else {
            $pesan = "User, Data Gagal Ditambahkan";
            $this->session->set_flashdata('pesan_ggl', $pesan);
            redirect('back_user');
        }
    }

    public function edit($id=0) {
        $data = array(
            'styleExtra'    => $this->load->view('user/style_user', '', true),
            'scriptExtra'   => $this->load->view('user/script_user', '', true),
            'user'          => $this->user->getbyid($id),
            'id'            => $id,
        );
        $this->_render('user/edit',$data);
    }

    public function update() {
        
        if ($this->user->update($this->input->post())) {
            $pesan = "User Berhasil Diubah";
            $this->session->set_flashdata('pesan_sks', $pesan);
            redirect('back_user');
        } else {
            $pesan = "User, Data Gagal Diubah";
            $this->session->set_flashdata('pesan_ggl', $pesan);
            redirect('back_user');
        }
    }
    
    public function delete($id=0) {
        if ($this->user->delete($id)) {
            $pesan = "User Berhasil Dihapus";
            $this->session->set_flashdata('pesan_sks', $pesan);
            redirect('back_user');
        } else {
            $pesan = "User, Data Gagal Dihapus";
            $this->session->set_flashdata('pesan_ggl', $pesan);
            redirect('back_user');
        }
    }
    
    public function datatables() {
        $data=$this->user->user_list();
        echo json_encode($data);
    }

}
