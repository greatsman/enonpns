<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends Secure {

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('login') == FALSE){
            $this->session->set_flashdata('msg_warning', 'Session telah kadaluarsa, silahkan login ulang.');
            redirect('Login');
        }
        $this->load->model('Pegawai_model','pegawai',true);
        $this->load->model('Opd_model','opd',true);
        $this->load->model('Divisi_model','divisi',true);
        $this->load->library('upload');
    }

    public function index()
    {
        $data = array(
            'styleExtra'        => $this->load->view('pegawai/style', '', true),
            'scriptExtra'       => $this->load->view('pegawai/script', '', true)
        );

        $this->_render('pegawai/list', $data);
    }

    private function fvalidation() {
        $this->load->library('form_validation');
        if($this->input->post('nik') != $original_value) {
           $is_unique =  '|is_unique[karyawan.nik]';
        } else {
           $is_unique =  '';
        }

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('nik', 'Nik', 'required|trim');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('pendidikan', 'Pendidikan', 'required');
        $this->form_validation->set_rules('id_opd', 'id_opd', 'required');
        $this->form_validation->set_rules('id_divisi', 'id_divisi', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');


        $this->form_validation->set_message('required', '%s masih kosong, silahkan isi');
        $this->form_validation->set_message('is_unique', '%s sudah digunakan, input %s lain');
        $this->form_validation->set_error_delimiters('<span class="help-block">&nbsp;</span>');
    }

    function datatables(){
        $data = $this->pegawai->get_all();
        echo json_encode($data);
    }


    public function add()
    {
        $this->fvalidation();
        if ($this->form_validation->run() == TRUE) {

            $config['upload_path'] = './uploads/foto/pegawai/'; 
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; 
            $config['encrypt_name'] = TRUE;

            $this->upload->initialize($config);
            if (!empty($_FILES['file_upload']['name'])) {

                if ($this->upload->do_upload('file_upload')) {
                    $gbr = $this->upload->data();

                    //Compress Image
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './uploads/foto/pegawai/' . $gbr['file_name'];
                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = FALSE;
                    $config['new_image'] = './uploads/foto/pegawai/' . $gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $gambar = $gbr['file_name'];
                } else {
                    $gambar = 'default.jpg';
                }
            } else {
                $gambar = 'default.jpg';
            }

            $data = array(
                'nama'              => $this->input->post('nama'),
                'nik'               => $this->input->post('nik'),
                'tempat_lahir'      => $this->input->post('tempat_lahir'),
                'tanggal_lahir'     => $this->input->post('tanggal_lahir'),
                'jabatan'           => $this->input->post('jabatan'),
                'alamat'            => $this->input->post('alamat'),
                'pendidikan'        => $this->input->post('pendidikan'),
                'id_opd'            => $this->input->post('id_opd'),
                'id_divisi'         => $this->input->post('id_divisi'),
                'jenis_kelamin'     => $this->input->post('jenis_kelamin'),
                'file'              => $gambar,
            );

            $pesan = "Pegawai Berhasil Ditambahkan";
            $this->session->set_flashdata('pesan_sks', $pesan);
            $status = $this->pegawai->save($data);
            redirect('Pegawai');

        }else{

            $data = array(
                'opd'           => $this->opd->get_all(),
                'divisi'        => $this->divisi->get_all(),
                'styleExtra'    => $this->load->view('pegawai/style', '', true),
                'scriptExtra'   => $this->load->view('pegawai/script', '', true),

            );
            $this->_render('pegawai/add',$data);
        }
    }

    public function edit(){
        $id = $this->uri->segment(3);
        $row = $this->pegawai->get($id);
        $this->fvalidation();

        $data = array(
            'opd'               => $this->opd->get_all(),
            'divisi'            => $this->divisi->get_all(),
            'styleExtra'        => $this->load->view('pegawai/style', '', true),
            'scriptExtra'       => $this->load->view('pegawai/script_edit', '', true),
            'dt'                => $row,      
        );
        $this->_render('pegawai/edit',$data);
    }


    function get_data_edit(){
        $id = $this->input->post('id',TRUE);
        $data = $this->pegawai->get_pegawai($id)->result();
        echo json_encode($data);
    }


    public function update()
    {
        $id = $this->uri->segment(3);

        $this->fvalidation();
        if ($this->form_validation->run() == TRUE) {
            $data = array(
                'nama'              => $this->input->post('nama'),
                'nik'               => $this->input->post('nik'),
                'tempat_lahir'      => $this->input->post('tempat_lahir'),
                'tanggal_lahir'     => $this->input->post('tanggal_lahir'),
                'jabatan'           => $this->input->post('jabatan'),
                'alamat'            => $this->input->post('alamat'),
                'pendidikan'        => $this->input->post('pendidikan'),
                'id_opd'            => $this->input->post('id_opd'),
                'id_divisi'         => $this->input->post('id_divisi'),
                'jenis_kelamin'     => $this->input->post('jenis_kelamin')
            );
            $status = $this->pegawai->update($id, $data);



            if (!empty($_FILES['file_upload']['name'])) {
                $config['upload_path'] = './uploads/foto/pegawai/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; 
                $config['encrypt_name'] = TRUE;

                $this->upload->initialize($config);
                if (!empty($_FILES['file_upload']['name'])) {

                    if ($this->upload->do_upload('file_upload')) {
                        $gbr = $this->upload->data();
                        //Compress Image
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = './uploads/foto/pegawai/' . $gbr['file_name'];
                        $config['create_thumb'] = FALSE;
                        $config['maintain_ratio'] = FALSE;
                        $config['new_image'] = './uploads/foto/pegawai/' . $gbr['file_name'];
                        $this->load->library('image_lib', $config);
                        $this->image_lib->resize();

                        $gambar = $gbr['file_name'];
                    } else {
                        $gambar = 'default.jpg';
                    }
                } else {
                    $gambar = 'default.jpg';
                }

                $Path = FCPATH.'uploads\foto\pegawai\\'.$this->input->post('file_lama');
                unlink($Path);


                $data3 = array(
                    'file'          => $gambar,
                );

                $status1 = $this->pegawai->update($id, $data3);
            }


            $pesan = "Pegawai Berhasil Diupdate";
            $this->session->set_flashdata('pesan_sks', $pesan);
            redirect('Pegawai');

        }else{
            $data = array(
                'opd'           => $this->opd->get_all(),
                'styleExtra'    => $this->load->view('pegawai/style', '', true),
                'scriptExtra'   => $this->load->view('pegawai/script_edit', '', true),

            );
            $this->_render('pegawai/add',$data);
        }

    }

    public function delete() {
        $id=$this->input->post('id');
        $row = $this->pegawai->get($id);
        if ($row->file != 'default.jpg') {
            $Path = FCPATH.'uploads\foto\pegawai\\'.$row->file;
            unlink($Path);
        }

        $data=$this->pegawai->delete($id);
        echo json_encode($data);
    }

    function get_sub_divisi(){
        $opd_id = $this->input->post('id',TRUE);
        $data = $this->divisi->get_sub_divisi($opd_id)->result();
        echo json_encode($data);
    }


}
