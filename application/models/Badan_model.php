<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Badan_model extends CI_Model
{
    private $tbl = 'ref_jenis_badan';

    function __construct()
    {
        parent::__construct();
    }

    function get_all()
    {
        $this->db->where('id != 0');
        $query = $this->db->get($this->tbl);
        if ($query->num_rows() !== 0) {
            return $query->result();
        } else
            return FALSE;
    }

    function get($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get($this->tbl);
        if ($query->num_rows() !== 0) {
            return $query->row();
        } else
            return FALSE;
    }

    //-- admin
    function save($data)
    {
        $this->db->insert($this->tbl, $data);
        return $this->db->insert_id();
    }

    function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($this->tbl, $data);
    }

    function delete($id)
    {
        $hasil = $this->db->query("DELETE FROM ref_jenis_badan WHERE id='$id'");
        return $hasil;
    }
}

/* End of file _model.php */