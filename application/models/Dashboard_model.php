<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
    }


    function get_all_lembaga(){
    	$sql = "
            select o.nama, count(d.id) as jml
			from opd o
			left join divisi d on o.id=d.id_opd
			where id_jenis_badan = 2
			group by o.nama";

        $query = $this->db->query($sql);
        if($query->num_rows()!==0)
        {
            return $query->result();
        }
        else
            return FALSE;
    }

    function get_sum_lembaga(){
    	$sql = "
			select count(d.*) as jml
			from divisi d 
			join opd o on d.id_opd=o.id
			where o.id_jenis_badan=2";

        $query = $this->db->query($sql);
        if($query->num_rows()!==0)
        {
            return $query->row();
        }
        else
            return FALSE;
    }

    function get_sum_opd(){
    	$sql = "
			select count(d.*)  as jml
			from divisi d 
			join opd o on d.id_opd=o.id
			where o.id_jenis_badan=1";

        $query = $this->db->query($sql);
        if($query->num_rows()!==0)
        {
            return $query->row();
        }
        else
            return FALSE;
    }


    function get_all_opd(){
    	$sql = "
            select o.id, o.nama, count(d.id) as jml
			from opd o
			left join divisi d on o.id=d.id_opd
			where id_jenis_badan = 1
			group by 1, 2";

        $query = $this->db->query($sql);
        if($query->num_rows()!==0)
        {
            return $query->result();
        }
        else
            return FALSE;
    }


    function get_all_karyawan_jk(){
    	$sql = "
            select jenis_kelamin, count(*) as jml
			from karyawan
			group by jenis_kelamin";

        $query = $this->db->query($sql);
        if($query->num_rows()!==0)
        {
            return $query->result();
        }
        else
            return FALSE;
    }

    function get_all_karyawan(){
    	$sql = "
            select count(*) as jml
			from karyawan";

        $query = $this->db->query($sql);
        if($query->num_rows()!==0)
        {
            return $query->row();
        }
        else
            return FALSE;
    }

    function get_pegawai_search($nama, $id_opd)
    {
        if ($id_opd == 'opd') {
            $sql = "
                select k.*, d.nama nama_divisi, o.nama nama_opd  
                from karyawan k 
                join divisi d on k.id_divisi=d.id
                join opd o on k.id_opd=o.id
                where k.nama ilike '%".$nama."%'
                order by o.id DESC"
            ;
            
        }else{
            $sql = "
                select k.*, d.nama nama_divisi, o.nama nama_opd  
                from karyawan k 
                join divisi d on k.id_divisi=d.id
                join opd o on k.id_opd=o.id
                where k.id_opd = ".$id_opd."
                and k.nama ilike '%".$nama."%'
                order by o.id DESC"
            ;
        }
        $query = $this->db->query($sql);
        if($query->num_rows()!==0)
        {
            return $query->result();
        }
        else
            return FALSE;
    }

    function get_all_opd_ref(){
        $sql = "
            select o.id, o.nama, count(d.id) as jml
            from opd o
            left join divisi d on o.id=d.id_opd
            group by 1, 2";

        $query = $this->db->query($sql);
        if($query->num_rows()!==0)
        {
            return $query->result();
        }
        else
            return FALSE;
    }
    

    function get_pegawai_detail($id)
    {

        $sql = "
            select k.*, d.nama nama_divisi, o.nama nama_opd  
            from karyawan k 
            join divisi d on k.id_divisi=d.id
            join opd o on k.id_opd=o.id
            where k.id =".$id
        ;
        $query = $this->db->query($sql);
        if($query->num_rows()!==0)
        {
            return $query->row();
        }
        else
            return FALSE;
    }

}