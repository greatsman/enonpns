<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model 
{
    public function login($username, $password)
    {
        $this->db->select('id, id_karyawan,username, password, role')
                 ->from('users')
                 ->where('username', $username)
                 ->where('password', $password);
        $query = $this->db->get();

        return $query;
    }

    function get_id($id)
    {

        $this->db->where('id',$id);
        $query = $this->db->get('karyawan');
        if($query->num_rows()!==0)
        {
            return $query->row();
        }
        else
            return FALSE;
    }

    function get_id_baru($id)
    {

        $sql = "
            select id_opd, id_divisi, o.id_jenis_badan as id_badan
            from karyawan k 
            join opd o on k.id_opd = o.id 
            ";

        $query = $this->db->query($sql);
        if($query->num_rows()!==0)
        {
            return $query->row();
        }
        else
            return FALSE;
    }

    public function save($post){
        $data = array(
            'username'  => $post['username'], 
            'fullname'  => $post['fullname'],
            'password'  => md5($post['password']),
            'email'     => $post['email'],
            'is_admin'  => 2,
        );
        return $this->db->insert('pengguna', $data);
    }
}