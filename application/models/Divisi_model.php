<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Divisi_model extends CI_Model {
    private $tbl = 'divisi';
    
    function __construct() {
        parent::__construct();
    }
    
    function get_all()
    {

        if ($this->session->userdata('role') == 0) {
            $sql = "
                select d.*, o.nama as nama_opd
                from divisi d left join opd o on d.id_opd=o.id order by o.id DESC";
        }elseif ($this->session->userdata('role') == 1) {
            $id = $this->session->userdata('id_opd');
            $sql = "
                select d.*, o.nama as nama_opd
                from divisi d 
                left join opd o on d.id_opd=o.id 
                where o.id='$id' order by o.id DESC";
        }else{
            $id = $this->session->userdata('id_opd');
            $sql = "
                select d.*, o.nama as nama_opd
                from divisi d left join opd o on d.id_opd=o.id where o.id='$id' order by o.id DESC";
        }

        $query = $this->db->query($sql);
        if($query->num_rows()!==0)
        {
            return $query->result();
        }
        else
            return FALSE;
    }

    function get_sub_divisi($opd_id){
        $query = $this->db->get_where($this->tbl, array('id_opd' => $opd_id));
        return $query;
    }
    
    function get($id)
    {
        $this->db->where('id',$id);
        $query = $this->db->get($this->tbl);
        if($query->num_rows()!==0)
        {
            return $query->row();
        }
        else
            return FALSE;
    }

    //-- admin
    function save($data) {
        $this->db->insert($this->tbl,$data);
        return $this->db->insert_id();
    }
    
    function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->tbl,$data);
    }
    
    function delete($id) {
        $hasil=$this->db->query("DELETE FROM divisi WHERE id='$id'");
        return $hasil;
    }
}

/* End of file _model.php */