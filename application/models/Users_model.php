<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model
{
    private $tbl = 'users';

    function __construct()
    {
        parent::__construct();
    }

    function get_all()
    {
        if ($this->session->userdata('role') == 0) {
            $sql = "
                select u.*, 
                CASE 
                WHEN k.nama = null THEN u.username
                ELSE username END as nama_pegawai, 
                CASE
                WHEN u.role =0 THEN 'Superadmin'
                WHEN u.role =1 THEN 'Admin Dinas'
                WHEN u.role =2 THEN 'Operator Dinas'
                ELSE 'Pegawai' END as rolenya
                from users u
                left join karyawan k on u.id_karyawan = k.id
                order by u.id DESC";
        }elseif ($this->session->userdata('role') == 1) {
            $id = $this->session->userdata('id_opd');
            $sql = "
                select u.*, 
                CASE 
                WHEN k.nama = null THEN u.username
                ELSE username END as nama_pegawai, 
                CASE
                WHEN u.role =0 THEN 'Superadmin'
                WHEN u.role =1 THEN 'Admin Dinas'
                WHEN u.role =2 THEN 'Operator Dinas'
                ELSE 'Pegawai' END as rolenya
                from users u
                left join karyawan k on u.id_karyawan = k.id
                join opd o on o.id = k.id_opd
                where u.role >='1'
                and o.id = '$id'
                order by u.id DESC";
        }elseif($this->session->userdata('role') == 2) {
            $sql = "
                select u.*, 
                CASE 
                WHEN k.nama = null THEN u.username
                ELSE username END as nama_pegawai, 
                CASE
                WHEN u.role =0 THEN 'Superadmin'
                WHEN u.role =1 THEN 'Admin Dinas'
                WHEN u.role =2 THEN 'Operator Dinas'
                ELSE 'Pegawai' END as rolenya
                from users u
                left join karyawan k on u.id_karyawan = k.id
                where u.role =='2'
                order by u.id DESC";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() !== 0) {
            return $query->result();
        } else
            return FALSE;
    }

    function get($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get($this->tbl);
        if ($query->num_rows() !== 0) {
            return $query->row();
        } else
            return FALSE;
    }

    //-- admin
    function save($data)
    {
        $this->db->insert($this->tbl, $data);
        return $this->db->insert_id();
    }

    function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($this->tbl, $data);
    }

    function delete($id)
    {
        $hasil = $this->db->query("DELETE FROM users WHERE id='$id'");
        return $hasil;
    }
}

/* End of file _model.php */