<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Opd_model extends CI_Model {
    private $tbl = 'opd';
    
    function __construct() {
        parent::__construct();
    }
    
    function get_all()
    {
        if ($this->session->userdata('role') == 0) {
            $sql = "
                select o.*, jb.nama as nama_badan
                from opd o left join ref_jenis_badan jb on o.id_jenis_badan=jb.id order by o.id desc";

        }elseif($this->session->userdata('role') == 1){
            $id = $this->session->userdata('id_opd');
            $sql = "
                select o.*, jb.nama as nama_badan
                from opd o left join ref_jenis_badan jb on o.id_jenis_badan=jb.id where o.id='$id' order by o.id desc";
        }else{
            $id = $this->session->userdata('id_opd');
            $sql = "
                select o.*, jb.nama as nama_badan
                from opd o left join ref_jenis_badan jb on o.id_jenis_badan=jb.id where o.id='$id' order by o.id desc";
        }

        $query = $this->db->query($sql);
        if($query->num_rows()!==0)
        {
            return $query->result();
        }
        else
            return FALSE;
    }
    
    function get($id)
    {
        $this->db->where('id',$id);
        $query = $this->db->get($this->tbl);
        if($query->num_rows()!==0)
        {
            return $query->row();
        }
        else
            return FALSE;
    }

    //-- admin
    function save($data) {
        $this->db->insert($this->tbl,$data);
        return $this->db->insert_id();
    }
    
    function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->tbl,$data);
    }
    
    function delete($id) {
        $hasil=$this->db->query("DELETE FROM opd WHERE id='$id'");
        return $hasil;
    }
}

/* End of file _model.php */