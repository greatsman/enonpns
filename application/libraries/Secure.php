<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Secure extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
	}

	private function sideMenu()
	{
		$hasil=array('bumbu');

		return $hasil;
	}


	public function _render($view, $data = array())
	{
		$data['sideMenus'] = $this->sideMenu();
		$this->load->view('template/header', $data, false);
		$this->load->view('template/sidebar', $data, false);
		$this->load->view($view, $data, false);
		$this->load->view('template/footer', $data, false);
	}

	public function _render_front($view, $data = array())
	{
		$this->load->view('front_end/template/header', $data, false);
		$this->load->view('front_end/template/navbar', $data, false);
		$this->load->view($view, $data, false);
		$this->load->view('front_end/template/footer', $data, false);
	}

	public function error_404()
	{
		$this->load->view('error_404');
	}
}