<style type="text/css">
  .user-info {
    max-width: 200px;
  }

  .navbar {
    background: #0f117d !important;
  }

  .ace-nav>li.light-blue>a {
    background-color: #2990a8 !important;
  }
</style>

<body class="no-skin">
  <div id="navbar" class="navbar navbar-default ace-save-state">
    <div class="navbar-container ace-save-state" id="navbar-container">
      <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
        <span class="sr-only">Toggle sidebar</span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>
      </button>

      <div class="navbar-header pull-left" style="margin-bottom: -8px;">
        <a href="<?php echo base_url() ?>dashboard" class="navbar-brand">
          <small>
            <img src="<?= base_url() ?>assets/img/logo.png" style="width: 35px;height: 40px">
            E-NON PNS Kab. Manokwari Selatan
          </small>
        </a>
      </div>

      <div class="navbar-buttons navbar-header pull-right" role="navigation">
        <ul class="nav ace-nav">

          <li class="light-blue dropdown-modal">
            <a data-toggle="dropdown" href="<?php echo base_url() ?>template/#" class="dropdown-toggle">
              <span class="user-info">
                <small>Selamat Datang,</small>
                <p>
                  <?= $this->session->userdata('username') ?></p>

              </span>

              <i class="ace-icon fa fa-caret-down"></i>
            </a>

            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

              <li>
                <a href="<?= base_url('login/logout') ?>">
                  <i class="ace-icon fa fa-power-off"></i>
                  Logout
                </a>
              </li>
            </ul>
          </li>

        </ul>
      </div>
    </div><!-- /.navbar-container -->
  </div>

  <div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
      try {
        ace.settings.loadState('main-container')
      } catch (e) {}
    </script>

    <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
      <script type="text/javascript">
        try {
          ace.settings.loadState('sidebar')
        } catch (e) {}
      </script>

      <!--         <div class="sidebar-shortcuts" id="sidebar-shortcuts">

        </div> -->
      <!-- /.sidebar-shortcuts -->

      <ul class="nav nav-list">
        <?php if ($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2) { ?>
        <li class="<?php if ($this->uri->segment(1) == "dashboard") {
                      echo "active";
                    } ?>">
          <a href="<?php echo base_url() ?>dashboard">
            <i class="menu-icon fa fa-tachometer"></i>
            <span class="menu-text"> Dashboard</span>
          </a>

          <b class="arrow"></b>
        </li>
        <?php } ?>

        <?php if ($this->session->userdata('role') != 0 ) { ?>
        <li class="<?php if ($this->uri->segment(1) == "biodata") {
                      echo "active";
                    } ?>">
          <a href="<?php echo base_url() ?>biodata">
            <i class="menu-icon fa fa-user"></i>
            <span class="menu-text"> Biodata User</span>
          </a>

          <b class="arrow"></b>
        </li>
        <?php } ?>
        
        <?php if ($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2) { ?>
        <li class="<?php if ($this->uri->segment(1) == "kategori_opd" || $this->uri->segment(1) == "divisi" || $this->uri->segment(1) == "pegawai") {
                      echo "active open";
                    } ?>">
          <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-desktop"></i>
            <span class="menu-text">
              Menu Utama
            </span>

            <b class="arrow fa fa-angle-down"></b>
          </a>

          <b class="arrow"></b>

          <ul class="submenu nav-show" style="display: block;">
          <?php if ($this->session->userdata('role') == 0) { ?>
            <li class="<?php if ($this->uri->segment(1) == "badan") {
                          echo "active";
                        } ?>">
              <a href="<?php echo base_url() ?>badan">
                <i class="menu-icon fa fa-caret-right"></i>
                Jenis Badan
                <b class="arrow"></b>
              </a>

              <b class="arrow"></b>
            </li>
          <?php } ?>

          <?php if ($this->session->userdata('role') == 0) { ?>
            <li class="<?php if ($this->uri->segment(1) == "opd") {
                          echo "active";
                        } ?>">
              <a href="<?php echo base_url() ?>opd">
                <i class="menu-icon fa fa-caret-right"></i>
                OPD
                <b class="arrow"></b>
              </a>

              <b class="arrow"></b>
            </li>
          <?php } ?>

          <?php if ($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2) { ?>
            <li class="<?php if ($this->uri->segment(1) == "divisi") {
                          echo "active";
                        } ?>">
              <a href="<?php echo base_url() ?>divisi">
                <i class="menu-icon fa fa-caret-right"></i>
                Divisi OPD
                <b class="arrow"></b>
              </a>

              <b class="arrow"></b>
            </li>

            <li class="<?php if ($this->uri->segment(1) == "pegawai") {
                          echo "active";
                        } ?>">
              <a href="<?php echo base_url() ?>pegawai">
                <i class="menu-icon fa fa-caret-right"></i>
                Pegawai
                <b class="arrow"></b>
              </a>

              <b class="arrow"></b>
            </li>


          <?php if ($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1) { ?>
            <li class="<?php if ($this->uri->segment(1) == "users") {
                          echo "active";
                        } ?>">
              <a href="<?php echo base_url() ?>users">
                <i class="menu-icon fa fa-caret-right"></i>
                Users
                <b class="arrow"></b>
              </a>

              <b class="arrow"></b>
            </li>
          <?php } ?>
          </ul>
        </li>
        <?php } ?>
        <?php } ?>

      <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
      </div>
    </div>