<style type="text/css">
.user-info{
    max-width: 200px;

</style>

  <body class="no-skin">
    <div id="navbar" class="navbar navbar-default          ace-save-state">
      <div class="navbar-container ace-save-state" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
          <span class="sr-only">Toggle sidebar</span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>
        </button>

        <div class="navbar-header pull-left">
          <a href="<?php echo base_url()?>dashboard" class="navbar-brand">
            <small>
              KBM OP BBWS Bengawan Solo
            </small>
          </a>
        </div>

        <div class="navbar-buttons navbar-header pull-right" role="navigation">
          <ul class="nav ace-nav">

            <li class="light-blue dropdown-modal">
              <a data-toggle="dropdown" href="<?php echo base_url()?>template/#" class="dropdown-toggle">
                <span class="user-info">
                  <small>Selamat Datang,</small>
                  <p><? echo $this->session->userdata('username') ?> ( <? echo $this->session->userdata('role_name'); ?> )</p>

                </span>

                <i class="ace-icon fa fa-caret-down"></i>
              </a>

              <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

                <li>
                  <a href="<?=base_url('login/logout') ?>">
                    <i class="ace-icon fa fa-power-off"></i>
                    Logout
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div><!-- /.navbar-container -->
    </div>

    <div class="main-container ace-save-state" id="main-container">
      <script type="text/javascript">
        try{ace.settings.loadState('main-container')}catch(e){}
      </script>

      <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <script type="text/javascript">
          try{ace.settings.loadState('sidebar')}catch(e){}
        </script>

<!--         <div class="sidebar-shortcuts" id="sidebar-shortcuts">

        </div> -->
        <!-- /.sidebar-shortcuts -->

        <ul class="nav nav-list">
          <? 
            if ($arrAccess['isShowPengguna']) {
              $active = "";
              if ($this->uri->segment(1)== "pengguna"){
                    $active = "active";
              };
              
              echo '

                      <li class="'.$active.'">
                        <a href="'.base_url().'pengguna">
                          <i class="menu-icon glyphicon glyphicon-user"></i>
                          <span class="menu-text"> Pengguna </span>
                        </a>

                        <b class="arrow"></b>
                      </li>
                    ';
            }; 

          ?>
          <li class="<?php if($this->uri->segment(1)=="profil_desa"){echo "active open";}?>">
            <a href="#" class="dropdown-toggle">
              <i class="menu-icon fa fa-desktop"></i>
              <span class="menu-text">
                Menu Utama
              </span>

              <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu nav-show" style="display: block;">
              <li class="<?php if($this->uri->segment(1)=="profil_desa"){echo "active";}?>">
                <a href="<?php echo base_url()?>profil_desa">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Profil Desa
                  <b class="arrow"></b>
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php if($this->uri->segment(1)=="profil_p3a"){echo "active";}?>" >
                <a href="<?php echo base_url()?>profil_p3a">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Profil P3A
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php if($this->uri->segment(1)=="data_tpm"){echo "active";}?>">
                <a href="<?php echo base_url()?>data_tpm">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Data TPM
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php if($this->uri->segment(1)=="tahapan_persiapan"){echo "active";}?>">
                <a href="<?php echo base_url()?>tahapan_persiapan">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Tahapan Persiapan
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php if($this->uri->segment(1)=="tahapan_penyelesaian"){echo "active";}?>">
                <a href="<?php echo base_url()?>tahapan_penyelesaian">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Tahapan Penyelesaian
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php if($this->uri->segment(1)=="progress_fiskeu"){echo "active";}?>">
                <a href="<?php echo base_url()?>progress_fiskeu">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Progres Fisik & Keuangan
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php if($this->uri->segment(1)=="manfaat"){echo "active";}?>">
                <a href="<?php echo base_url()?>manfaat" >
                  <i class="menu-icon fa fa-caret-right"></i>
                  Manfaat
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php if($this->uri->segment(1)=="jenis_kegiatan"){echo "active";}?>">
                <a href="<?php echo base_url()?>jenis_kegiatan">
                  <i class="menu-icon fa fa-caret-right"></i>
                    
                    Jenis Kegiatan
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php if($this->uri->segment(1)=="dokumentasi"){echo "active";}?>">
                <a href="<?php echo base_url()?>dokumentasi">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Dokumentasi
                  <b class="arrow"></b>
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php if($this->uri->segment(1)=="unggulan"){echo "active";}?>">
                <a href="<?php echo base_url()?>unggulan">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Unggulan
                  <b class="arrow"></b>
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php if($this->uri->segment(1)=="lokasi_blm_diakomodir"){echo "active";}?>">
                <a href="<?php echo base_url()?>lokasi_blm_diakomodir">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Lokasi belum diakomodir
                  <b class="arrow"></b>
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php if($this->uri->segment(1)=="koordinat"){echo "active";}?>">
                <a href="<?php echo base_url()?>koordinat">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Koordinat Desa
                  <b class="arrow"></b>
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php if($this->uri->segment(1)=="summary"){echo "active";}?>" >
                <a href="<?php echo base_url()?>summary">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Summary
                  <b class="arrow"></b>
                </a>

                <b class="arrow"></b>
              </li>

            </ul>

          </li>
            <li class="<?php if($this->uri->segment(1) == "temp_excel"){echo "active";}?>">
              <a href="<?php echo base_url()?>temp_excel">
                <i class="menu-icon glyphicon glyphicon-file"></i>
                <span class="menu-text"> Template Excel </span>
              </a>

              <b class="arrow"></b>
            </li>

            <li class="<?php if($this->uri->segment(1) == "panduan-aplikasi"){echo "active";}?>">
              <a href="/manual/Manual-KbmOp.pdf" target="_blank">
                <i class="menu-icon glyphicon glyphicon-list-alt"></i>
                <span class="menu-text"> Panduan Aplikasi </span>
              </a>

              <b class="arrow"></b>
            </li>
        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
          <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
      </div>