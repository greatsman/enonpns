<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Login - E-NON PNS</title>

    <meta name="description" content="User login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="shortcut icon" href="<?=base_url()?>assets/img/logo.png">
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="<?=base_url('template/assets/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" href="<?=base_url('template/assets/font-awesome/4.5.0/css/font-awesome.min.css')?>" />

    <!-- text fonts -->
    <link rel="stylesheet" href="<?=base_url('template/assets/css/fonts.googleapis.com.css')?>" />

    <!-- ace styles -->
    <link rel="stylesheet" href="<?=base_url('template/assets/css/ace.min.css')?>" />

    <!--[if lte IE 9]>
      <link rel="stylesheet" href="assets/css/ace-part2.min.css')?>" />
    <![endif]-->
    <link rel="stylesheet" href="<?=base_url('template/assets/css/ace-rtl.min.css')?>" />

    <!--[if lte IE 9]>
      <link rel="stylesheet" href="assets/css/ace-ie.min.css')?>" />
    <![endif]-->

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->

  <style type="text/css">
    .vertical-center {
      margin: 0;
      /*position: absolute;*/
      top: 100px;
      /*-ms-transform: translateY(-50%);*/
      /*transform: translateY(-50%);*/
    }

    .bandd{

      background: white;
      border-top-style: solid; 
      border-bottom-style: solid;
      /*border-top: 5;*/
      /*border-bottom: 20px;*/
      border-top-width: 20;
      border-bottom-width: 20; 
      border-top-color: #d71149;
      border-bottom-color: #d71149;
    }
    .gbr{
      -webkit-box-shadow: 0px 1px 4px 0.5px rgba(247,247,247,1);
      -moz-box-shadow: 0px 1px 4px 0.5px rgba(247,247,247,1);
      box-shadow: 0px 1px 4px 0.5px rgba(247,247,247,1);
    }
    .kothak-gambar{
      background: white;
      margin-top: 12px;


    }
    .kothak-profil{
      background: white;
      margin-top: 12px;
      margin-bottom: 12px;

      padding-right: 12px;
      padding-left: 12px;

      padding-top: 12px;
      padding-bottom: 12px;

    }

    .center-cropped {
        overflow: hidden;
        max-height: 200px;
    }
    .center-cropped img {
        width: 100%;
        position: relative;
    }
    .judul{
      text-align: left; 
      font-size: 30px;
      background-color: white;

      margin-top: 1px;
      margin-left: -12px;
      margin-right: -12px;
      margin-bottom: -2px;

      padding-right: 8px;
      padding-left: 8px;
      padding-top: 30px;
      padding-bottom: 8px;

    }
    .light-login {
      animation: fadeIn .5s forwards;
      animation-delay: 0.5s; /* optional */
      opacity: 0;
    }

    @keyframes fadeIn {
      to {
        opacity: 1;
      }
    }
    </style>
  </head>

  <body class="login-layout light-login">
    <div class="main-container">
      <div class="main-content">

        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <div class="login-container">
              <div class="center">
                <h1>
                <img src="<?=base_url('assets/img/logo.png')?>" style="width: 30%;height: 30%">
                <br>
                  <span class="black">E-NON PNS</span>
                </h1>
                <h2>
                  <span class="black" id="id-text2">Kab. Manokwari Selatan</span>
                </h2>
              </div>
              <div class="col-lg-5 col-md-5 col-xs-12">
                <div class="login-container">
                  <div class="space-6"></div>
                   <?php
                    $pesan_sks = $this->session->flashdata('pesan_sks');
                    $pesan_ggl = $this->session->flashdata('pesan_ggl');
                    ?>
                    <?php if (! empty($pesan_sks)) : ?>
                    <div class="alert alert-block alert-success">
                      <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                      </button>
                      <p>
                        <strong>
                          <i class="ace-icon fa fa-check"></i>
                         Berhasil!
                        </strong>
                       <?php echo $pesan_sks ;?>
                      </p>
                    </div>
                    <?php endif ?>
                    <?php if (! empty($pesan_ggl)) : ?>
                    <div class="alert alert-danger">
                          <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                          </button>

                          <strong>
                            <i class="ace-icon fa fa-times"></i>
                            Peringatan!
                          </strong>
                          <br>
                           <?php echo $pesan_ggl ;?>
                          <br />
                        </div>
                  <?php endif ?>

                  <div class="position-relative">
                    <div id="login-box" class="login-box visible widget-box no-border">
                      <div class="widget-body">
                        <div class="widget-main">
                          <h4 class="header red lighter bigger" style="text-align: center;">
                            <i class="ace-icon fa fa-lock red"></i>
                            Login <?= $content  ?>
                          </h4>

                          <div class="space-6"></div>

                          <form action="<?=base_url('login/login');?>" method="post">
                            <fieldset>
                              <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                  <input type="text" class="form-control"  name="username" placeholder="Username" />
                                  <i class="ace-icon fa fa-user"></i>
                                </span>
                              </label>

                              <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                  <input type="password" class="form-control" name="password" placeholder="Password" />
                                  <i class="ace-icon fa fa-lock"></i>
                                </span>
                              </label>
                              <div class="space"></div>

                              <div class="clearfix">
                                <button type="submit" class="btn btn-sm btn-primary" style="width: 300px">
                                  <i class="ace-icon fa fa-key"></i>
                                  <span class="bigger-110">Login</span>
                                </button>
                              </div>

                              <div class="space-4"></div>
                            </fieldset>
                          </form>
                        </div><!-- /.widget-main -->
                      </div><!-- /.widget-body -->
                        <div class="toolbar clearfix">
                          <div>
                            <a href="<?= base_url();  ?>" data-target="#forgot-box" class="forgot-password-link">
                              <i class="ace-icon fa fa-arrow-left"></i>
                              Kembali Ke Beranda
                            </a>
                          </div>
                        </div>
                    </div><!-- /.login-box -->
                  </div><!-- /.position-relative -->
                </div>
              </div><!-- /.col -->


        </div><!-- /.row -->
      </div><!-- /.main-content -->
    </div><!-- /.main-container -->

    <!-- basic scripts -->

    <!--[if !IE]> -->
    <script src="<?=base_url('template/assets/js/jquery-2.1.4.min.js')?>"></script>
    <script src="<?=base_url('template/assets/js/bootstrap.min.js')?>"></script>

    <!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
      <script src="assets/js/excanvas.min.js"></script>
    <![endif]-->
    <script src="<?=base_url('template/assets/js/jquery-ui.custom.min.js')?>"></script>
    <script src="<?=base_url('template/assets/js/jquery.ui.touch-punch.min.js')?>"></script>
    <script src="<?=base_url('template/assets/js/bootbox.js')?>"></script>
    <script src="<?=base_url('template/assets/js/jquery.easypiechart.min.js')?>"></script>
    <script src="<?=base_url('template/assets/js/jquery.gritter.min.js')?>"></script>
    <script src="<?=base_url('template/assets/js/spin.js')?>"></script>

    <script src="<?=base_url('template/assets/js/jquery-numeric/jquery.numeric.min.js')?>"></script>

    <!-- ace scripts -->
    <script src="<?=base_url('template/assets/js/ace-elements.min.js')?>"></script>
    <script src="<?=base_url('template/assets/js/ace.min.js')?>"></script>

    <!-- inline scripts related to this page -->
    <script type="text/javascript">
      jQuery(function($) {
        /**
        $('#myTab a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
          //console.log(e.target.getAttribute("href"));
        })
          
        $('#accordion').on('shown.bs.collapse', function (e) {
          //console.log($(e.target).is('#collapseTwo'))
        });
        */
        
        $('#myTab a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
          //if($(e.target).attr('href') == "#home") doSomethingNow();
        })
      
        
        /**
          //go to next tab, without user clicking
          $('#myTab > .active').next().find('> a').trigger('click');
        */
      
      
        $('#accordion-style').on('click', function(ev){
          var target = $('input', ev.target);
          var which = parseInt(target.val());
          if(which == 2) $('#accordion').addClass('accordion-style2');
           else $('#accordion').removeClass('accordion-style2');
        });
        
        //$('[href="#collapseTwo"]').trigger('click');
      

        $('#tahun_baru').numeric({decimal: false, negative: false});
        $('#tahap_baru').numeric({decimal: false, negative: false});
        $('#btn_tahap_baru').click(function (){
          $('#tahun_baru').prop('disabled', false);
          $('#tahap_baru').prop('disabled', false);
          $('#tahun_baru').show();
          $('#tahap_baru').show();

        })
      
        $('.easy-pie-chart.percentage').each(function(){
          $(this).easyPieChart({
            barColor: $(this).data('color'),
            trackColor: '#EEEEEE',
            scaleColor: false,
            lineCap: 'butt',
            lineWidth: 8,
            animate: ace.vars['old_ie'] ? false : 1000,
            size:75
          }).css('color', $(this).data('color'));
        });
      
        $('[data-rel=tooltip]').tooltip();
        $('[data-rel=popover]').popover({html:true});
      
      
        $('#gritter-regular').on(ace.click_event, function(){
          $.gritter.add({
            title: 'This is a regular notice!',
            text: 'This will fade out after a certain amount of time. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#" class="blue">magnis dis parturient</a> montes, nascetur ridiculus mus.',
            image: 'assets/images/avatars/avatar1.png', //in Ace demo ./dist will be replaced by correct assets path
            sticky: false,
            time: '',
            class_name: (!$('#gritter-light').get(0).checked ? 'gritter-light' : '')
          });
      
          return false;
        });
      
        $('#gritter-sticky').on(ace.click_event, function(){
          var unique_id = $.gritter.add({
            title: 'This is a sticky notice!',
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#" class="red">magnis dis parturient</a> montes, nascetur ridiculus mus.',
            image: 'assets/images/avatars/avatar.png',
            sticky: true,
            time: '',
            class_name: 'gritter-info' + (!$('#gritter-light').get(0).checked ? ' gritter-light' : '')
          });
      
          return false;
        });
      
      
        $('#gritter-without-image').on(ace.click_event, function(){
          $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'This is a notice without an image!',
            // (string | mandatory) the text inside the notification
            text: 'This will fade out after a certain amount of time. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#" class="orange">magnis dis parturient</a> montes, nascetur ridiculus mus.',
            class_name: 'gritter-success' + (!$('#gritter-light').get(0).checked ? ' gritter-light' : '')
          });
      
          return false;
        });
      
      
        $('#gritter-max3').on(ace.click_event, function(){
          $.gritter.add({
            title: 'This is a notice with a max of 3 on screen at one time!',
            text: 'This will fade out after a certain amount of time. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#" class="green">magnis dis parturient</a> montes, nascetur ridiculus mus.',
            image: 'assets/images/avatars/avatar3.png', //in Ace demo ./dist will be replaced by correct assets path
            sticky: false,
            before_open: function(){
              if($('.gritter-item-wrapper').length >= 3)
              {
                return false;
              }
            },
            class_name: 'gritter-warning' + (!$('#gritter-light').get(0).checked ? ' gritter-light' : '')
          });
      
          return false;
        });
      
      
        $('#gritter-center').on(ace.click_event, function(){
          $.gritter.add({
            title: 'This is a centered notification',
            text: 'Just add a "gritter-center" class_name to your $.gritter.add or globally to $.gritter.options.class_name',
            class_name: 'gritter-info gritter-center' + (!$('#gritter-light').get(0).checked ? ' gritter-light' : '')
          });
      
          return false;
        });
        
        $('#gritter-error').on(ace.click_event, function(){
          $.gritter.add({
            title: 'This is a warning notification',
            text: 'Just add a "gritter-light" class_name to your $.gritter.add or globally to $.gritter.options.class_name',
            class_name: 'gritter-error' + (!$('#gritter-light').get(0).checked ? ' gritter-light' : '')
          });
      
          return false;
        });
          
      
        $("#gritter-remove").on(ace.click_event, function(){
          $.gritter.removeAll();
          return false;
        });
          
      
        ///////
      
      
        $("#bootbox-regular").on(ace.click_event, function() {
          bootbox.prompt("What is your name?", function(result) {
            if (result === null) {
              
            } else {
              
            }
          });
        });
          
        $("#bootbox-confirm").on(ace.click_event, function() {
          bootbox.confirm("Are you sure?", function(result) {
            if(result) {
              //
            }
          });
        });
        
      /**
        $("#bootbox-confirm").on(ace.click_event, function() {
          bootbox.confirm({
            message: "Are you sure?",
            buttons: {
              confirm: {
               label: "OK",
               className: "btn-primary btn-sm",
              },
              cancel: {
               label: "Cancel",
               className: "btn-sm",
              }
            },
            callback: function(result) {
              if(result) alert(1)
            }
            }
          );
        });
      **/
        
      
        $("#bootbox-options").on(ace.click_event, function() {
          bootbox.dialog({
            message: "<span class='bigger-110'>I am a custom dialog with smaller buttons</span>",
            buttons:
            {
              "success" :
               {
                "label" : "<i class='ace-icon fa fa-check'></i> Success!",
                "className" : "btn-sm btn-success",
                "callback": function() {
                  //Example.show("great success");
                }
              },
              "danger" :
              {
                "label" : "Danger!",
                "className" : "btn-sm btn-danger",
                "callback": function() {
                  //Example.show("uh oh, look out!");
                }
              }, 
              "click" :
              {
                "label" : "Click ME!",
                "className" : "btn-sm btn-primary",
                "callback": function() {
                  //Example.show("Primary button");
                }
              }, 
              "button" :
              {
                "label" : "Just a button...",
                "className" : "btn-sm"
              }
            }
          });
        });
      
      
      
        $('#spinner-opts small').css({display:'inline-block', width:'60px'})
      
        var slide_styles = ['', 'green','red','purple','orange', 'dark'];
        var ii = 0;
        $("#spinner-opts input[type=text]").each(function() {
          var $this = $(this);
          $this.hide().after('<span />');
          $this.next().addClass('ui-slider-small').
          addClass("inline ui-slider-"+slide_styles[ii++ % slide_styles.length]).
          css('width','125px').slider({
            value:parseInt($this.val()),
            range: "min",
            animate:true,
            min: parseInt($this.attr('data-min')),
            max: parseInt($this.attr('data-max')),
            step: parseFloat($this.attr('data-step')) || 1,
            slide: function( event, ui ) {
              $this.val(ui.value);
              spinner_update();
            }
          });
        });
      
      
      
        //CSS3 spinner
        $.fn.spin = function(opts) {
          this.each(function() {
            var $this = $(this),
              data = $this.data();
      
            if (data.spinner) {
            data.spinner.stop();
            delete data.spinner;
            }
            if (opts !== false) {
            data.spinner = new Spinner($.extend({color: $this.css('color')}, opts)).spin(this);
            }
          });
          return this;
        };
      
        function spinner_update() {
          var opts = {};
          $('#spinner-opts input[type=text]').each(function() {
            opts[this.name] = parseFloat(this.value);
          });
          opts['left'] = 'auto';
          $('#spinner-preview').spin(opts);
        }
      
      
      
        $('#id-pills-stacked').removeAttr('checked').on('click', function(){
          $('.nav-pills').toggleClass('nav-stacked');
        });
      
        
        
        
        
        
        ///////////
        $(document).one('ajaxloadstart.page', function(e) {
          $.gritter.removeAll();
          $('.modal').modal('hide');
        });
      
      });
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
    <script type="text/javascript">
      if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>

    <!-- inline scripts related to this page -->
    <script type="text/javascript">
      jQuery(function($) {
       $(document).on('click', '.toolbar a[data-target]', function(e) {
        e.preventDefault();
        var target = $(this).data('target');
        $('.widget-box.visible').removeClass('visible');//hide others
        $(target).addClass('visible');//show target
       });
      });
      
      
      
      //you don't need this, just used for changing background
      jQuery(function($) {
       $('#btn-login-dark').on('click', function(e) {
        $('body').attr('class', 'login-layout');
        $('#id-text2').attr('class', 'white');
        $('#id-company-text').attr('class', 'blue');
        
        e.preventDefault();
       });
       $('#btn-login-light').on('click', function(e) {
        $('body').attr('class', 'login-layout light-login');
        $('#id-text2').attr('class', 'grey');
        $('#id-company-text').attr('class', 'blue');
        
        e.preventDefault();
       });
       $('#btn-login-blur').on('click', function(e) {
        $('body').attr('class', 'login-layout blur-login');
        $('#id-text2').attr('class', 'white');
        $('#id-company-text').attr('class', 'light-blue');
        
        e.preventDefault();
       });
       
      });
    </script>
    <script type="text/javascript">
      function check_pass() {
          if (document.getElementById('password').value ==
                  document.getElementById('confirm_password').value) {
              document.getElementById('register').disabled = false;
          } else {
              document.getElementById('register').disabled = true;
          }
      }
    </script>
  </body>
</html>
