<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon glyphicon glyphicon-th"></i>
          <a href="#">Edit OPD</a>
      </ul><!-- /.breadcrumb -->
    </div>
    <div class="page-content"> 
      <?php $this->load->view('template/alert'); ?>
      <div class="widget-box">
      <div class="widget-header">
        <h4 class="widget-title">Edit OPD</h4>
      </div>

      <div class="widget-body">
        <div class="widget-main">
              <div class="row">
            <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <form  action="<?php echo base_url(); ?>Opd/update/<?= $dt->id ?>" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
              <div class="form-group <?= form_error('nama') ? 'has-error' : null ?>">
                <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Nama</label>

                <div class="col-sm-9">
                  <input type="text" id="nama" name="nama" placeholder="Nama" class="col-xs-10 col-sm-12" value="<?= $dt->nama ?>"/>
                  <?= form_error('nama') ?>
                </div>
              </div>

              <div class="form-group <?= form_error('id_jenis_badan') ? 'has-error' : null ?>">
                <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Jenis Badan</label>

                <div class="col-sm-9">
                  <select class="form-control" id="id_jenis_badan" name="id_jenis_badan">
                    <?php foreach ($badan as $bd) { ?>
                    <option value="<?= $bd->id ?>" <?php if($dt->id_jenis_badan==$bd->id) echo 'selected';?>><?= $bd->nama ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="clearfix form-actions">
                <div class="col-md-offset-2 col-md-12">
                  <button class="btn btn-xs btn-info pull-left" type="submit">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Simpan
                  </button>

                  &nbsp; &nbsp; &nbsp;
                  <a href="<?= base_url('Opd') ?>" class="btn btn-xs btn-warning" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Kembali
                  </a>
                </div>
              </div>
            </form>
            </div>
          </div>
        </div>
      </div>
    </div>
   </div>

 </div>
</div>