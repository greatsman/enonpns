<style type="text/css">
    input[type=file]{
      display: inline;
    }
    #image_preview{
      padding: 10px;
    }
    #image_preview img{
      max-width: 100%;
      padding: 5px;
    }
    #img-upload{
      max-width: 100%;
      padding: 5px;
    }
    table.dataTable {
      margin: 0;
    }


    .combo_search {
      /*float: left;*/
      height:30px;
      width:117px;
      margin-top:-1px;
    }

    .date_search{
      /*float: left;*/
      height:30px;
      width:200px;
      margin-top:-1px;
    }

    .border_head{
      color: #455966;
      border: 0.5px solid #f9f9f9;
      text-align:center;
      padding: :0.5px;
    }
  </style>