<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon glyphicon glyphicon-th"></i>
          <a href="#">Kategori OPD</a>
      </ul><!-- /.breadcrumb -->
    </div>
    <div class="page-content"> 
      <?php $this->load->view('template/alert'); ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="clearfix">
            <div>
              <a href="<?= base_url('Opd/add') ?>" class="btn btn-sm btn-success "><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah OPD</a>
            </div>
            <div class="pull-right tableTools-container"></div>
          </div>
          <div class="table-header">
            List Data Kategori OPD
          </div>

          <!-- div.table-responsive -->

          <!-- div.dataTables_borderWrap -->
          <div>
            <table id="mydata" class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama OPD</th>
                  <th>Nama Badan</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="show_data">
              </tbody>
            </table>
          </div>
        </div>
      </div>
   </div>

 </div>
</div>


<div class="modal fade" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Hapus OPD</h4>
      </div>
      <form class="form-horizontal">
        <div class="modal-body">

          <input type="hidden" name="kode" id="textkode" value="">
          <div class="alert alert-warning">
            <p>Apakah Anda yakin mau menghapus OPD ini?</p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          <button class="btn_hapus btn btn-danger" id="btn_hapus">Hapus</button>
        </div>
      </form>
    </div>
  </div>
</div>