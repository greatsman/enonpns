<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Download Template Excel</a>
				</li>
			</ul><!-- /.breadcrumb -->

		</div>

		<div class="page-content">

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					<br></br>
					<br></br>
					<form class="form-horizontal">
		              <div class="form-group">
		                <label class="col-sm-3 control-label no-padding-right"> Berkas : </label>
		                <div class="col-sm-9">

							<select id="combo_excel" name="combo_excel" style="width: 300px"></label>'+
							  <option value="profil_desa">Profil Desa</option>
							  <option value="profil_p3a">Profil P3A</option>
							  <option value="data_tpm">Data TPM</option>
							  <option value="tahapan_persiapan">Tahapan Persiapan</option>
							  <option value="tahapan_penyelesaian">Tahapan Penyelesaian</option>
							  <option value="progress_fiskeu">Progres Fisik&Keuangan</option>
							  <option value="manfaat">Manfaat</option>
							  <option value="jenis_kegiatan">Jenis Kegiatan</option>
							  <option value="dokumentasi">Dokumentasi</option>
							  <option value="unggulan">Unggulan</option>
							  <option value="rekap_usulan_lokasi">Rekap Usulan Lokasi</option>
							  <option value="koordinat_desa">Koordinat Desa</option>
							  <option value="pemanfaatan_dana">Pemanfaatan Dana</option>
							</select>
							<button id="bt_download" class="btn btn-sm btn-info" type="button" style="height: 30px; margin-top: -3px">
								Download
							</button>
		                </div>
		              </div>
					</form>
					</div><!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
	</div>
</div><!-- /.main-content -->
