

<script type="text/javascript" src="<?=base_url('template/datatables/js/jquery.dataTables.js')?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        table = $('#table').DataTable({ 
            // "searching": true,
            "processing": true,
            "serverSide": true,
            // "fixedColumns": true,
            "order": [],
            "scrollX": true,
            "ajax": {
                "url": "<?php echo site_url('Pengguna/ajax_list')?>",
                "type": "POST"
            },
            "columnDefs": [

            ],

            "columns": [
                {"visible": false, "width": 1},
                {"orderable": false, "width": 30},
                {"orderable": false, "width": "30%"},
                {"orderable": false, "width": "30%"},
                {"orderable": false, "width": "30%"},
                {"orderable": false, "width": "30%"},
                {"orderable": false, "width": 100},
                {"orderable": false, "width": "30%"},
            ],

            "aoColumnDefs": [
               {
                    "aTargets": [7],
                    "mData": null,
                    "mRender": function (data, type, full) {
                        return getAksiBtn(data);
                    }
                }
             ],

        });

        // -------------- start of list functions
        $('.item_hapus').on('click',function(){
            return confirm('Anda yakin menghapus pengguna ini ?');
        });

        function getAksiBtn(data){
            var btns = '<button  onclick="ConfirmDelete('+data[0]+')" class="btn btn-minier btn-danger item_hapus">Delete</button>';
            var btns = btns + '<button id="doubah"  onclick="doUbah('+ data[0] +')" class="btn btn-minier btn-yellow">Ubah</button>';
            return btns;

        }
 
    });

    function doUbah(id){
        // window.open()
        window.open("<?=base_url('Pengguna/edit/')?>" + id);
        // alert("mengubah" + id);
    }

    function ConfirmDelete(id)
    {
        var x = confirm("Yakin Menghapus Pengguna ini ?");
        if (x) {
            window.location.href = "<?=base_url('Pengguna/delete/')?>"+id; 
            return true;
        } else {
            return false;
        }
    }

    // ------------------------------ end of list functions

    // -------------------------------start of edit functions
    function passwordGantiCheck(){
        var isUbah = document.getElementById('isUbahPassword').checked;
        var cupassword = document.getElementById('cpassword').value;
        $("#password").prop('disabled', !isUbah);
        if (isUbah){
            $('#password').val('');
        }else{
            $('#password').val(cupassword);
        }
    }
    // ------------------------------- end of edit functions
 
</script>