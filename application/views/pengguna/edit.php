<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Ubah Pengguna</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					<form action="<?php echo base_url(); ?>Pengguna/update" method="post" enctype="multipart/form-data"  id="submit" class="form-horizontal" role="form">
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="fullname"> Nama Lengkap </label>
							<div class="col-sm-9">
								<input type="hidden" class="form-control" id="id" name="id" placeholder="Masukkan nama" value="<?= $pengguna->id ?>">
								<input type="text" id="fullname" name="fullname" placeholder="Fullname" class="col-xs-10 col-sm-5" value="<?= $pengguna->fullname ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="username"> Pengguna </label>

							<div class="col-sm-9">
								<input type="text" id="username" name="username" placeholder="Username" class="col-xs-10 col-sm-5" value="<?= $pengguna->username ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="username"> Password </label>

							<div class="col-sm-9">
								<input type="hidden" id="cpassword" value="<?= $pengguna->password ?>">
								<input type="password" id="password" name="password" placeholder="Password" class="col-xs-10 col-sm-5" value="<?= $pengguna->password ?>" disabled/>
								&nbsp;&nbsp;<input type="checkbox" id="isUbahPassword" name="isUbahPassword" value=1 onclick="passwordGantiCheck()">&nbsp;Reset Password
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Email </label>

							<div class="col-sm-9">
								<input type="email" id="email" name="email" placeholder="Email" class="col-xs-10 col-sm-5" value="<?= $pengguna->email ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Hak Akses </label>

							<div class="col-xs-12 col-sm-6">
								<div class="control-group">

									<div class="radio">
										<label>
											<input value=0 name="is_admin" type="radio" class="ace" <? echo $superAdminChecked; ?>/>
											<span class="lbl">Super Admin</span>
										</label>
									</div>

									<div class="radio">
										<label>
											<input value=1 name="is_admin" type="radio" class="ace" <? echo $adminChecked; ?>/>
											<span class="lbl">Admin</span>
										</label>
									</div>

									<div class="radio">
										<label>
											<input value=2 name="is_admin" type="radio" class="ace" <? echo $pengunjungChecked; ?>/>
											<span class="lbl">Pengunjung</span>
										</label>
									</div>
								</div>
							</div>
						</div>

						<div class="clearfix form-actions">
							<div class="col-md-offset-3 col-md-9">
								<button id="submit" class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>

								&nbsp; &nbsp; &nbsp;
								<a href="<?= base_url('Pengguna') ?>" class="btn btn-xs btn-warning" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
									Kembali
								</a>
							</div>
						</div>
					</form>
					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
