<link rel="stylesheet" type="text/css" href="<?=base_url('template/datatables/css/jquery.dataTables.css')?>">
<style type="text/css">
    input[type=file]{
      display: inline;
    }
    #image_preview{
      padding: 10px;
    }
    #image_preview img{
      max-width: 100%;
      padding: 5px;
    }
    #img-upload{
      max-width: 100%;
      padding: 5px;
    }

    .border_head{
      color: #455966;
      border: 0.5px solid #f9f9f9;
      text-align:center;
    }
  </style>