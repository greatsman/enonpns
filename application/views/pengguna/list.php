<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Pengguna</a>
        </li>
      </ul><!-- /.breadcrumb -->
    </div>

    <div class="page-content">
      <?php $this->load->view('template/alert'); ?>
      <div class="row">
         <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                <div class="post">
                  <div class="user-block">
                    <a href="<?= base_url('Pengguna/add')?>" class="btn btn-sm btn-info">Tambah Pengguna</a>
                  </div>
                  <div class="row margin-bottom">
                    <div class="hr hr-16 hr-dotted"></div>
                    <div class="box-body">
                       <div id="reload">
                          <table id="table" class="display" cellspacing="0" >
                            <thead>
                              <tr style="background-color: #d4d8dd">
                                <th class="border_head" >id</th>
                                <th class="border_head" >No</th>
                                <th class="border_head" >Nama Lengkap</th>
                                <th class="border_head" >Username</th>
                                <th class="border_head" >Password</th>
                                <th class="border_head" >Email</th>
                                <th class="border_head" >Hak Akses</th>
                                <th class="border_head" >Aksi</th>
                              </tr>
                          </thead>
                          <tbody>
                          </tbody>
                          </table>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
            </div>
          </div>
        <!-- /.col -->
      </div>
    
   </div>
 </div>
</div>