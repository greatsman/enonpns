<script src="<?=base_url('template/assets/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('template/assets/js/jquery.dataTables.bootstrap.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    tampil_data();
    get();

    $('#mydata').dataTable();

    function tampil_data() {
      $.ajax({
        type: 'GET',
        url: '<?= base_url('Pegawai/datatables') ?>',
        async: false,
        dataType: 'json',
        success: function(data) {
          var html = '';
          var i;
          for (i = 0; i < data.length; i++) {
              html += '<tr>' +
                '<td style="word-wrap: break-word;">' + parseInt(i+1) + '</td>' +
                '<td style="word-wrap: break-word;">' + data[i].nama + '</td>' +
                '<td style="word-wrap: break-word;">' + data[i].nama_opd + '</td>' +
                '<td style="word-wrap: break-word;">' + data[i].nama_divisi + '</td>' +
                '<td style="word-wrap: break-word;">' + data[i].nik + '</td>' +
                '<td style="word-wrap: break-word;">' + data[i].jabatan + '</td>' +
                '<td style="word-wrap: break-word;">' + data[i].pendidikan + '</td>' +
                '<td style="word-wrap: break-word;">' + data[i].alamat + '</td>' +
                '<td style="word-wrap: break-word;text-align:center"><img width="80px" height="100px" class="img-resposive img-thumbnail" src="./uploads/foto/pegawai/' + data[i].file +'"> </td>' +
                '<td style="word-wrap: break-word;text-align:center;">' +
                '<a href="javascript:;" class="btn btn-xs btn-info item_edit" data="' + data[i].id + '"><i class="ace-icon fa fa-pencil bigger-120"></i></a>' +
                '<a href="javascript:;" class="btn btn-xs btn-danger item_hapus" data="' + data[i].id + '"><i class="ace-icon fa fa-trash-o bigger-120"></i></a>' +
                '</td>' +
                '</tr>';
          }
          $('#show_data').html(html);
        }

      });
    }

    function get() {
      var id=$('#id_opd').val();
        $.ajax({
            url : "<?php echo site_url('Pegawai/get_sub_divisi');?>",
            method : "POST",
            data : {id: id},
            async : true,
            dataType : 'json',
            success: function(data){
                
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<option value='+data[i].id+'>'+data[i].nama+'</option>';
                }
                $('#id_divisi').html(html);
            }
        });
        return false;
    }
    $('#show_data').on('click', '.item_hapus', function() {
      var id = $(this).attr('data');
      $('#ModalHapus').modal('show');
      $('[name="kode"]').val(id);
    });

    $('#btn_hapus').on('click', function() {
      var id = $('#textkode').val();
      $.ajax({
        type: "POST",
        url: "<?= base_url('Pegawai/delete') ?>",
        dataType: "JSON",
        data: {
          id: id
        },
        success: function(data) {
          $('#ModalHapus').modal('hide');
          swal("Sukses!", "Hapus Data Berhasil!", "success");
          location.reload();
        }
      });
      return false;
    });
    $('#show_data').on('click', '.item_edit', function() {
      var id = $(this).attr('data');
      window.location.href = "<?php echo site_url('Pegawai/edit/'); ?>" + id;
    });

    $('#show_data').on('click', '.item_detail', function() {
      var id = $(this).attr('data');
      console.log(id);
      window.location.href = "<?php echo site_url('Pegawai/detailnya/'); ?>" + id;
    });
    $('#id_opd').change(function(){ 
        var id=$(this).val();
        $.ajax({
            url : "<?php echo site_url('Pegawai/get_sub_divisi');?>",
            method : "POST",
            data : {id: id},
            async : true,
            dataType : 'json',
            success: function(data){
                
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<option value='+data[i].id+'>'+data[i].nama+'</option>';
                }
                $('#id_divisi').html(html);
            }
        });
        return false;
    }); 

  });
</script>