<script type="text/javascript">
	$(document).ready(function(){
        get_data_edit();
		 $('#id_opd').change(function(){ 
            var id=$(this).val();
            var id_divisi = $('#id_divisi').val();
            $.ajax({
                url : "<?php echo site_url('Pegawai/get_sub_divisi');?>",
                method : "POST",
                data : {id: id},
                async : true,
                dataType : 'json',
                success: function(data){

                    $('select[name="id_divisi"]').empty();

                    $.each(data, function(key, value) {
                        if(id_divisi==value.id){
                            $('select[name="id_divisi"]').append('<option value="'+ value.id +'" selected>'+ value.nama +'</option>').trigger('change');
                        }else{
                            $('select[name="id_divisi"]').append('<option value="'+ value.id +'">'+ value.nama +'</option>');
                        }
                    });

                }
            });
            return false;
        });

        //load data for edit
        function get_data_edit(){
            var id = <?= $this->uri->segment(3) ?> ;
            $.ajax({
                url : "<?php echo site_url('Pegawai/get_data_edit');?>",
                method : "POST",
                data :{id :id},
                async : true,
                dataType : 'json',
                success : function(data){
                    $.each(data, function(i, item){
                        $('[name="id_opd"]').val(data[i].id_opd).trigger('change');
                        $('[name="id_divisi"]').val(data[i].id_divisi).trigger('change');
                    });
                }

            });
        }

	});
</script>