<script src="<?=base_url('template/assets/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('template/assets/js/jquery.dataTables.bootstrap.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    tampil_data();

    $('#mydata').dataTable();

    function tampil_data() {
      $.ajax({
        type: 'GET',
        url: '<?= base_url('Users/datatables') ?>',
        async: false,
        dataType: 'json',
        success: function(data) {
          var html = '';
          var i;
          for (i = 0; i < data.length; i++) {
            if (data[i].username == "sa") {
              html += '<tr>' +
                '<td style="word-wrap: break-word;">' + parseInt(i+1) + '</td>' +
                '<td style="word-wrap: break-word;">' + data[i].username + '</td>' +
                '<td style="word-wrap: break-word;">' + data[i].rolenya + '</td>' +
                '<td style="word-wrap: break-word;text-align:center;">' +
                '</td>' +
                '</tr>';
            }else{
              html += '<tr>' +
                '<td style="word-wrap: break-word;">' + parseInt(i+1) + '</td>' +
                '<td style="word-wrap: break-word;">' + data[i].username + '</td>' +
                '<td style="word-wrap: break-word;">' + data[i].rolenya + '</td>' +
                '<td style="word-wrap: break-word;text-align:center;">' +
                '<a href="javascript:;" class="btn btn-xs btn-info item_edit" data="' + data[i].id + '"><i class="ace-icon fa fa-pencil bigger-120"></i></a>' +
                '<a href="javascript:;" class="btn btn-xs btn-danger item_hapus" data="' + data[i].id + '"><i class="ace-icon fa fa-trash-o bigger-120"></i></a>' +
                '</td>' +
                '</tr>';
            }
          }
          $('#show_data').html(html);
        }

      });
    }
    $('#show_data').on('click', '.item_hapus', function() {
      var id = $(this).attr('data');
      $('#ModalHapus').modal('show');
      $('[name="kode"]').val(id);
    });

    $('#btn_hapus').on('click', function() {
      var id = $('#textkode').val();
      $.ajax({
        type: "POST",
        url: "<?= base_url('Users/delete') ?>",
        dataType: "JSON",
        data: {
          id: id
        },
        success: function(data) {
          $('#ModalHapus').modal('hide');
          swal("Sukses!", "Hapus Data Berhasil!", "success");
          location.reload();
        }
      });
      return false;
    });
    $('#show_data').on('click', '.item_edit', function() {
      var id = $(this).attr('data');
      window.location.href = "<?php echo site_url('Users/edit/'); ?>" + id;
    });

    $('#show_data').on('click', '.item_detail', function() {
      var id = $(this).attr('data');
      console.log(id);
      window.location.href = "<?php echo site_url('Users/detailnya/'); ?>" + id;
    });


  });
</script>