<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon glyphicon glyphicon-th"></i>
          <a href="#">Edit Akses Users</a>
      </ul><!-- /.breadcrumb -->
    </div>
    <div class="page-content">
      <?php $this->load->view('template/alert'); ?>
      <div class="widget-box">
        <div class="widget-header">
          <h4 class="widget-title">Edit Akses Users</h4>
        </div>

        <div class="widget-body">
          <div class="widget-main">
            <div class="row">
              <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <form action="<?php echo base_url(); ?>Users/update/<?= $dt->id ?>" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
                  <input type="hidden" id="id_karyawan" name="id_karyawan" placeholder="id_karyawan" class="col-xs-12 col-sm-12" value="<?= $dt->id_karyawan ?>" />
                  <?= form_error('id_karyawan') ?>
                  <div class="form-group <?= form_error('username') ? 'has-error' : null ?>">
                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Username</label>

                    <div class="col-sm-9">
                      <input type="text" id="username" name="username" placeholder="username" class="col-xs-12 col-sm-12" value="<?= $dt->username ?>" />
                      <?= form_error('username') ?>
                    </div>
                  </div>

                  <div class="form-group <?= form_error('password') ? 'has-error' : null ?>">
                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Password</label>

                    <div class="col-sm-9">
                      <input type="password" id="password" name="password" placeholder="isi Password" class="col-xs-12 col-sm-12" value="<?= $dt->password ?>" />
                      <?= form_error('password') ?>
                    </div>
                  </div>

                  <div class="form-group <?= form_error('role') ? 'has-error' : null ?>">
                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Role</label>

                    <div class="col-sm-9">
                      <select class="chosen form-control" id="role" name="role" data-placeholder="Choose a State...">
                        <?php if($this->session->userdata('role') == 0) { ?>
                        <option value="0" <?php if ($dt->role == '0') echo 'selected'; ?>>Superadmin</option>
                        <?php } ?>
                        <option value="1" <?php if ($dt->role == '1') echo 'selected'; ?>>Admin</option>
                        <option value="2" <?php if ($dt->role == '2') echo 'selected'; ?>>Operator</option>
                        <option value="3" <?php if ($dt->role == '3') echo 'selected'; ?>>Pegawai</option>
                      </select>
                    </div>
                  </div>

                  <div class="clearfix form-actions">
                    <div class="col-md-offset-2 col-md-12">
                      <button class="btn btn-xs btn-info pull-left" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Simpan
                      </button>

                      &nbsp; &nbsp; &nbsp;
                      <a href="<?= base_url('Users') ?>" class="btn btn-xs btn-warning" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Kembali
                      </a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>