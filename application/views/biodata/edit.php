<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon glyphicon glyphicon-th"></i>
          <a href="#">Edit Pegawai</a>
        </ul><!-- /.breadcrumb -->
      </div>
      <div class="page-content"> 
        <?php $this->load->view('template/alert'); ?>
        <div class="widget-box">
          <div class="widget-header">
            <h4 class="widget-title">Edit Pegawai</h4>
          </div>

          <div class="widget-body">
            <div class="widget-main">
              <div class="row">
                <div class="col-xs-12">
                  <!-- PAGE CONTENT BEGINS -->
                  <form  action="<?php echo base_url(); ?>Biodata/update" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
                    <input type="hidden" id="file_lama" name="file_lama" class="col-xs-12 col-sm-12" value="<?= $dt->file ?>"/>
                    <div class="form-group <?= form_error('nama') ? 'has-error' : null ?>">
                      <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Nama</label>

                      <div class="col-sm-9">
                        <input type="text" id="nama" name="nama" placeholder="Nama Pegawai" class="col-xs-12 col-sm-12" value="<?= $dt->nama ?>" required="required"/>
                        <?= form_error('nama') ?>
                      </div>
                    </div>

                    <div class="form-group <?= form_error('tempat_lahir') ? 'has-error' : null ?>">
                      <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Tempat Lahir</label>

                      <div class="col-sm-3">
                        <input type="text" id="tempat_lahir" name="tempat_lahir" placeholder="isi Tempat Lahir" class="col-xs-12 col-sm-12" value="<?= $dt->tempat_lahir ?>" required="required"/>
                        <?= form_error('tempat_lahir') ?>
                      </div>
                      <div class="col-sm-6">
                        <label class="col-sm-4 control-label no-padding-left" for="form-field-1">Tanggal Lahir</label>
                        <input type="date" id="tanggal_lahir" name="tanggal_lahir" placeholder="isi Tanggal Lahir" class="col-xs-12 col-sm-8" value="<?= $dt->tanggal_lahir ?>"/>
                        <?= form_error('tanggal_lahir') ?>
                      </div>
                    </div>

                    <div class="form-group <?= form_error('nik') ? 'has-error' : null ?>">
                      <label class="col-sm-2 control-label no-padding-right" for="form-field-1">NIK</label>

                      <div class="col-sm-3">
                        <input type="text" id="nik" name="nik" placeholder="isi NIK" class="col-xs-12 col-sm-12" value="<?= $dt->nik ?>" required="required"/>
                        <?= form_error('nik') ?>
                      </div>
                      <div class="col-sm-6">
                        <label class="col-sm-4 control-label no-padding-left" for="form-field-1">Jenis Kelamin</label>
                        <div class="col-sm-8">
                          <select class="form-control col-xs-12 col-sm-12" id="jenis_kelamin" name="jenis_kelamin" data-placeholder="Choose a State...">
                            <option value="Laki-Laki" <?php if($dt->jenis_kelamin=="Laki-Laki") echo 'selected';?>>Laki-Laki</option>
                            <option value="Perempuan" <?php if($dt->jenis_kelamin=="Perempuan") echo 'selected';?>>Perempuan</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="form-group <?= form_error('jabatan') ? 'has-error' : null ?>">
                      <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Jabatan</label>

                      <div class="col-sm-9">
                        <input type="text" id="jabatan" name="jabatan" placeholder="isi Jabatan" class="col-xs-12 col-sm-12" value="<?= $dt->jabatan ?>" required="required"/>
                        <?= form_error('jabatan') ?>
                      </div>
                    </div>

                    <div class="form-group <?= form_error('pendidikan') ? 'has-error' : null ?>">
                      <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Pendidikan</label>

                      <div class="col-sm-9">
                        <input type="text" id="pendidikan" name="pendidikan" placeholder="isi Pendidikan Terakhir" class="col-xs-12 col-sm-12" value="<?= $dt->pendidikan ?>" required="required"/>
                        <?= form_error('pendidikan') ?>
                      </div>
                    </div>

                    <div class="form-group <?= form_error('alamat') ? 'has-error' : null ?>">
                      <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Alamat</label>

                      <div class="col-sm-9">
                        <textarea class="form-control" id="alamat" name="alamat" id="form-field-8" placeholder="Masukkan Alamat" required="required"><?= $dt->alamat  ?></textarea>
                        <?= form_error('alamat') ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="form-field-1">&nbsp;</label>
                      <div class="col-sm-9">
                        <img class="img-resposive img-thumbnail" width="80px" height="110px" src="<?=base_url()?>uploads/foto/pegawai/<?= $dt->file ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Ganti Foto</label>

                      <div class="col-sm-9">
                        <input type="file" id="foto" name="file_upload" id="file_upload" />
                      </div>
                    </div>

                    <div class="clearfix form-actions">
                      <div class="col-md-offset-2 col-md-12">
                        <button class="btn btn-xs btn-info pull-left" type="submit">
                          <i class="ace-icon fa fa-check bigger-110"></i>
                          Simpan
                        </button>

                        &nbsp; &nbsp; &nbsp;
                        <a href="<?= base_url('Biodata') ?>" class="btn btn-xs btn-warning" type="reset">
                          <i class="ace-icon fa fa-undo bigger-110"></i>
                          Kembali
                        </a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>