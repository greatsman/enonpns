<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Biodata User</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<div class="page-header">
				<h1>
					Biodata <?= $dt->nama ?>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<div>
						<div id="user-profile-1" class="user-profile row">
							<div class="col-xs-12 col-sm-3 center">
								<div>
									<span class="profile-picture">
										<img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="
									<?= base_url() ?>uploads/foto/pegawai/<?= $dt->file ?>" />
									</span>

									<div class="space-4"></div>

									<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
										<div class="inline position-relative">
											<a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown">
												<i class="ace-icon fa fa-circle light-green"></i>
												&nbsp;
												<span class="white"><?= $dt->nama ?></span>
											</a>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-9">
								<div class="space-12"></div>

								<div class="profile-user-info profile-user-info-striped">
									<div class="profile-info-row">
										<div class="profile-info-name"> Nama </div>

										<div class="profile-info-value">
											<span class="editable" id="username"><?= $dt->nama ?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> OPD </div>

										<div class="profile-info-value">
											<span class="editable" id="username"><?= $dt->nama_opd ?></span>
										</div>
									</div>
									<div class="profile-info-row">
										<div class="profile-info-name"> Divisi </div>

										<div class="profile-info-value">
											<span class="editable" id="username"><?= $dt->nama_divisi ?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> jenis Kelamin </div>

										<div class="profile-info-value">
											<span class="editable" id="username"><?= $dt->jenis_kelamin ?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Tempat Lahir </div>

										<div class="profile-info-value">
											<i class="fa fa-map-marker light-orange bigger-110"></i>
											<span class="editable" id="age"><?= $dt->tempat_lahir ?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Tanggal Lahir </div>

										<div class="profile-info-value">
											<span class="editable" id="age"><?= date("d-m-Y  ", strtotime($dt->tanggal_lahir)) ?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name">Jabatan</div>

										<div class="profile-info-value">
											<span class="editable" id="age"><?= $dt->jabatan ?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Alamat </div>

										<div class="profile-info-value">
											<i class="fa fa-map-marker light-orange bigger-110"></i>
											<span class="editable" id="country"><?= $dt->alamat ?></span>
										</div>
									</div>


									<div class="profile-info-row">
										<div class="profile-info-name"> Pendidikan </div>

										<div class="profile-info-value">
											<span class="editable" id="signup"><?= $dt->pendidikan ?></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix form-actions">
							<div class="col-md-offset-3 col-md-9">
								<a href="<?= base_url('Biodata/edit') ?>" class="btn btn-info" type="button">
									<i class="ace-icon fa fa-cog bigger-110"></i>
									Rubah Identitas
								</a>
							</div>
						</div>
					</div>
					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->