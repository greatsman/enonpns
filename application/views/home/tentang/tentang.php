  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tentang Kami Sekapur Sirih
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Profil</a></li>
        <li class="active">Tentang Kami</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view('template/alert'); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <form action="<?php echo base_url('back_home/tentang_submit'); ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                              <label>Judul</label>
                              <input name="judul" class="form-control" placeholder="Judul" value="<?php echo ($data) ? $data->judul : ""; ?>">
                            </div>
                            <div class="form-group">
                              <label>Isi Singkat</label><br>
                              <textarea id="editor1" name="isi_singkat" rows="5" cols="80">
                                  <?php echo ($data) ? $data->isi_singkat : "" ?>
                              </textarea>
                            </div>
                            <div class="form-group">
                              <label>Isi</label><br>
                              <textarea id="editor2" name="isi" rows="20" cols="80">
                                  <?php echo ($data) ? $data->isi : "" ?>
                              </textarea>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    Gambar :
                                    <?php
                                    if ($data) {
                                        ?>
                                        <img src="<?php echo base_url("uploads/media/$data->gambar"); ?>" alt="" width="100%"/>
                                        <?php
                                    }
                                    ?>
                                        <input type="file" class="form-control" name="gambar">
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <input type="submit" value="Simpan" class="btn btn-primary" style="margin-top: 25px;">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>