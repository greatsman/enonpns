  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        About Video
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">About Video</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view('template/alert'); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <form action="<?php echo base_url('back_home/about_submit'); ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                              <label>Judul</label>
                              <input name="judul" class="form-control" placeholder="Judul" value="<?php echo ($data) ? $data->judul : ""; ?>">
                            </div>
                            <div class="form-group">
                              <label>Link Video</label>
                              <input name="isi" class="form-control" placeholder="Link Video" value="<?php echo ($data) ? $data->isi : ""; ?>">
                            </div>
                            <div class="form-group">
                            </div>
                            <?php if ($data) { 
                                $embed = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe style=\"width:100%;height:315px;\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>",$data->isi);
                                echo $embed;
                                } ?>
                            <div class="clearfix"></div>
                            <input type="submit" value="Simpan" class="btn btn-primary" style="margin-top: 25px;">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>