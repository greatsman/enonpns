  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Home Slider
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Slider</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view('template/alert'); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="col-md-12"> 
                            <div class="user-block">
                                <div class="btn btn-info pull-right" style="margin-bottom: 10px;" data-toggle="modal" onclick="add()">Tambah</div> 
                            </div>
                            <div class="row margin-bottom">
                            <?php foreach ($data as $slider) { ?>
                              <div class="col-sm-3" align="center">
                                <img class="img-responsive pad" src="<?=base_url('uploads/slider/'.$slider->gambar)?>" alt="Photo">
                                <h4><?=$slider->caption?></h4>
                                <div class="col-sm-offset-2 col-sm-10">
                                  <a href="<?=base_url('back_home/slider_del/'.urlencode($slider->gambar))?>" class="btn btn-danger pull-left" onclick="return confirm('Apakah anda yakin akan menghapus ?')">Hapus</a>
                                </div>
                              </div>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>