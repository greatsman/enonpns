<!-- CK Editor -->
<script src="<?=base_url()?>/assets/bower_components/ckeditor/ckeditor.js"></script>
<!-- Slimscroll -->
<script src="<?=base_url()?>/assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- Modal user-->
<div class="modal fade" id="mdl_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="mdl_form_submit" action="<?php echo base_url('back_home/slider_add'); ?>" method="post" enctype="multipart/form-data">
                <input name="id" id="mdl_form_id" value="" hidden>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="mdl_title">Tambah Gambar</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Caption</label>
                        <div class="col-sm-8">
                            <input name="caption" class="form-control" placeholder="Caption">
                        </div>
                        <br><br>
                    </div>
                    <div class="form-group">
                        <label for="inputFile" class="col-sm-4 control-label">File Gambar</label>
                        <div class="col-sm-8">
                            <input required="required" type="file" class="form-control" id="inputFile" name="upload" onchange="loadFile(event)" accept=".jpg,.jpeg,.bmp,.png">
                        </div>
                    </div>
                    <br><br>
                    <img id="preview" src="#" alt="" style="max-width:100%;" >
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button id="btn_submit" type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
  function add() {
        $('#mdl_form').modal('show');
    }
      var loadFile = function(event) {
        var output = document.getElementById('preview');
        output.src = URL.createObjectURL(event.target.files[0]);
      };
  $(function () {
  })
</script>