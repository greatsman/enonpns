<div class="container" style="min-height:700px;">
  <div class="section-header"><h3 class=" text-center wow " style="font-size:35px;">Cari Pegawai</h3></div>
<div class="col-sm-12">
  <form action="<?php echo base_url(); ?>pencarian/cari" method="POST" class="form-horizontal" enctype="multipart/form-data"><center>
    <div class="form-group">
      <p class="col-sm-3 control-label">Nama Non PNS</p>
      <div class="col-lg-8 col-sm-8">
        <div class="input-group">
          <span class="input-group-addon input-lg">
          <i class="fa fa-search"></i></span>
        <input type="text" required autofocus class="form-control input-lg" id="nama" value="" placeholder="cari pegawai" name="nama" ></input>
        </div>
        <span align="left" style="color:red">*Masukkan Data Minimal 4 Karakter Huruf</span>
      </div>
    </div>
    <div class="form-group">
      <p class="col-sm-3 control-label">OPD</p>
      <div class="col-lg-6 col-sm-6">
        <select class="form-control select2 select-lg" required id="id_opd" name="id_opd" >
            <option value="opd">-- SEMUA OPD --</option>
            <?php foreach ($opd as $op) { ?>
               <option value="<?= $op->id ?>" ><?= $op->nama ?></option>
            <?php } ?>                          
        </select>
         <span align="left" style="color:red">-</span>
      </div>
       <div class="col-sm-2"><input type='submit' name='cari' value='Cari Data' class='btn btn-lg btn-primary'> </div>
    </div>
  </form>
</div>

<div class="col-sm-12">
</div>
</div> <br><br>
