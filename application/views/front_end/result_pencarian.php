<div class="container" style="min-height:700px;">
  <div class="section-header"><h3 class=" text-center wow " style="font-size:35px;">Cari Pegawai</h3></div>
<div class="col-sm-12">
  <form action="<?php echo base_url(); ?>pencarian/cari" method="POST" class="form-horizontal" enctype="multipart/form-data"><center>
    <div class="form-group">
      <p class="col-sm-3 control-label">Nama Non PNS</p>
      <div class="col-lg-8 col-sm-8">
        <div class="input-group">
          <span class="input-group-addon input-lg">
          <i class="fa fa-search"></i></span>
        <input type="text" required autofocus class="form-control input-lg" value="" id="nama" placeholder="cari pegawai" name="nama" ></input>
        </div>
        <span align="left" style="color:red">*Masukkan Data Minimal 4 Karakter Huruf</span>
      </div>
    </div>
    <div class="form-group">
      <p class="col-sm-3 control-label">OPD</p>
      <div class="col-lg-6 col-sm-6">
            <select class="form-control select2 select-lg" required id="id_opd" name="id_opd" >
                <option value="opd">-- SEMUA OPD --</option>
                <?php foreach ($opd as $op) { ?>
                   <option value="<?= $op->id ?>" ><?= $op->nama ?></option>
                <?php } ?>                          
            </select>
         <span align="left" style="color:red">-</span>
      </div>
       <div class="col-sm-2"><input type='submit' name='cari' value='Cari Data' class='btn btn-lg btn-primary'> </div>
    </div>
  </form>
</div>

<div class="col-sm-12">
    <hr>
    <p>Kata Kunci : " <strong style="font-size:20px"><?= $kunci ?></strong> " <label style="text-align: right;color: red" class="pull-right">.:. klik nama untuk melihat informasi.</label></p>
        </div>
        <table class="table table-striped table-bordered table-hover" width="100%">
            <thead style="background-color:#4C8A97; color:#fff;">
                <tr role="row">
                    <th width="5%">No.</th>
                    <th width="20%">Foto</th>
                    <th width="75%">Nama</th>
                </tr>
            </thead>
            <tbody>
              <?php $no=0; foreach ($pegawai as $pg) { $no++ ?>
                <tr>
                    <td><center><?= $no; ?></center></td>
                    <td><center><img src="<?= base_url() ?>/uploads/foto/pegawai/<?= $pg->file ?>" class="img-resposive img-thumbnail" width="100"></center></td>
                    <td min-height="150">
                        <a data-toggle="modal" data-target=".bs" onclick="detail('<?php echo $pg->id;?> ')" >
                        <h4><strong style="text-transform: uppercase;"><?= $pg->nama ?></strong></h4>            
                        <strong><?= $pg->nama_opd ?><strong><br>
                        <strong><strong><br><br>
                        <strong><?= $pg->jabatan ?><strong>
                        </strong></strong></strong></strong></strong></strong></a><strong><strong><strong>
                      </strong></strong></strong></td>
                </tr>
              <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<br><br>



</div><div class="modal fade bs" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div id="form"></div>
  </div>
</div>

<script type="text/javascript">
  function detail(id){
    $.ajax({
      url:"<?php echo  base_url();?>pencarian/detail/"+id,
      success: function(response){
        $("#form").html(response);
      },
      dataType:"html"
    });
    return false;
  }
</script>