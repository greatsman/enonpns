<div class="modal-content">
  <div class="modal-header kepala">
    <button type="button" class="pull-right btn btn-danger" data-dismiss="modal" aria-hidden="true">X</button>
    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-search"></i> Detail Pegawai "<?= $data->nama ?>"</h4>
  </div>
  <div class="modal-body">
  <div class="row">
    <div class="col-sm-4">
      <center><img src="<?= base_url() ?>/uploads/foto/pegawai/<?= $data->file ?>" class="img-resposive img-thumbnail" width="140"></center>
    </div>
    <div class="col-sm-8 ">
    <div class="col-xs-12 "><br>
      <h4><strong style="text-transform: uppercase;"><?= $data->nama ?></strong></h4>
      <?= $data->nama_opd ?><br>
      BAGIAN RUMAH TANGGA          <hr>
    </div>
    <div class="col-xs-12">
      <strong><p>Tempat, Tgl Lahir :</p></strong>
      <p><?= $data->tempat_lahir ?>, <?= date("d-m-Y  ", strtotime($data->tanggal_lahir)) ?></p>
      <strong><p>Alamat :</p></strong>
      <p><?= $data->alamat ?></p>
      <strong><p>Jabatan :</p></strong>
      <p><?= $data->jabatan ?></p><p>
      <strong></strong></p><p><strong>Pendidikan Terakhir :</strong></p>
      <p><?= $data->pendidikan ?></p>
    </div>
  </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
  </div>
</div>