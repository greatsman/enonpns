<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
  });
</script>
<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-6"> ©2020 BKD Kabupaten Manokwari Selatan. </div>
      <div class="col-sm-6">
        <ul class="social-icons">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</footer><!--/#footer-->

<!-- Javascript -->
<script type="text/javascript" src="<?=base_url()?>template/frontend/aset/bootstrap/bootstrap.min.js"></script><!--
<script type="text/javascript" src="aset/js/psidebar.js"></script>
<script type="text/javascript" src="aset/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="aset/js/bootstrap-transition.js"></script>
<script type="text/javascript" src="aset/metisMenu/metisMenu.min.js"></script>-->
<script type="text/javascript" src="<?=base_url()?>template/frontend/aset/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?=base_url()?>template/frontend/aset/js/dataTables.bootstrap.js"></script>
</body>

<!-- Mirrored from enonpns.kaltimbkd.info/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 02 May 2020 09:07:58 GMT -->
</html>