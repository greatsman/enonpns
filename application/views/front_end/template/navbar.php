<body>
<header id="header">
  <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
      <div class="container">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
        <div class=" text-center wow fadeInDown animated" style="visibility: visible; animation-name: pulse;">
              <a class="navbar-brand hidden-lg hidden-md hidden-sm" href="#"><img src="<?=base_url()?>assets/img/logo.png" alt="logo" align="center" class="img-resposive" width="90"> BKD Kab.MANSEL</a>
              <a class="navbar-brand hidden-xs" href="<?= base_url(); ?>"><img src="<?=base_url()?>assets/img/logo.png" alt="logo" align="center" class="img-resposive" width="90"></a> <p class="navbar-brand hidden-xs" style="padding-top:30px;">BADAN KEPEGAWAIAN DAERAH <br>KABUPATEN MANOKWARI SELATAN</p>
        </div>
          </div>  
          <div class="collapse navbar-collapse navbar-right">
              <ul class="nav navbar-nav">
                  <li class="scroll <?php if ($this->uri->segment(1) == "") {echo "active";} ?>"><a href="<?= base_url() ?>">Beranda</a></li>
                  <li class="scroll <?php if ($this->uri->segment(1) == "pencarian") {echo "active";} ?>"><a href="<?= base_url('pencarian')  ?>">Cari Pegawai</a></li>
                  <li class="scroll <?php if ($this->uri->segment(2) == "opd") {echo "active";} ?>"><a href="<?= base_url('login/opd')  ?>">Login OPD</a></li>
                  <li class="scroll <?php if ($this->uri->segment(2) == "pegawai") {echo "active";} ?>"><a href="<?= base_url('login/pegawai')  ?>">Login Pegawai</a></li>                                         
              </ul>
          </div>
      </div><!--/.container-->
  </nav><!--/nav-->
</header><!--/header-->