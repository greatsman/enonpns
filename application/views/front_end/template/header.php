<!DOCTYPE html>

<!-- Mirrored from enonpns.kaltimbkd.info/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 02 May 2020 09:07:40 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
    <link rel="icon" href="aset/img/logo.png" size="16x16 32x32" type="image/png" />
    <title>e-Non PNS</title>
    <meta name="author" content="AR" />
    <meta name="description" content="me" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/frontend/aset/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/frontend/aset/css/font-awesome.min.css"> 
    <link rel="stylesheet" media="screen"  href="<?=base_url()?>template/frontend/aset/css/dataTables.bootstrap.css">  
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/frontend/aset/jquery-ui/jquery-ui.css">    
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/frontend/aset/select/select2.min.css">   
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/frontend/aset/css/animate.min.css"> 
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/frontend/aset/css/style-me.css">                           

    <script type="text/javascript" src="<?=base_url()?>template/frontend/aset/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>template/frontend/aset/jquery-ui/jquery-ui.min.js"></script> 
    <script type="text/javascript" src="<?=base_url()?>template/frontend/aset/select/select2.min.js"></script> 

  <script>
  $(document).ready(function() {
    $( "#datepicker" ).datepicker({
      dateFormat:'yy-mm-dd',
      changeMonth: true,
      changeYear: true,
      yearRange: '1950:2016'    
    });
      $('#btn').click(function() {
      $("#datepicker").focus();
    });
  });
  </script>
  <script>
  $(document).ready(function() {
    $( "#datepicker2" ).datepicker({
      dateFormat:'yy-mm-dd',
      changeMonth: true,
      changeYear: true,
      yearRange: '1960:2050'    
    });
      $('#btn2').click(function() {
      $("#datepicker2").focus();
    });
  });
  </script>  
  <script>
  $(document).ready(function() {
    $( "#datepicker3" ).datepicker({
      dateFormat:'yy-mm-dd',
      changeMonth: true,
      changeYear: true,
      yearRange: '1960:2050'    
    });
    $('#btn3').click(function() {
    $("#datepicker3").focus();
    });
  });
  </script>
  <script>
  function checkAvailability(string) {
    $("#loaderIcon").show();
    jQuery.ajax({
    url: "check_availability.php",
    data:'username='+$("#username").val(),
    type: "POST",
    success:function(data){
      $("#user-availability-status").html(data);
      if(data != 0)
        { $('#username').val('');  }
      $("#loaderIcon").hide();
    },
    error:function (){}
    });
  }
  </script>
  <style type="text/css">
    .ui-widget-header{
      background-color:#4C8A97; 
    }
    p {font-size: 15px; }
  </style>
</head>