<script type="text/javascript">
var htmlobjek;
$(document).ready(function(){
  $("#skpd").change(function(){
    var skpd = $("#skpd").val();
    $.ajax({
        url: "root/ambildata.php",
        data: "skpd="+skpd,
        cache: false,
        success: function(msg){
            $("#bidang").html(msg);
            $("#sub_bidang").html(msg); }
    });
  });  
}); 
</script>
<style type="text/css">.big{font-size: 16px;}.badge{background-color: #34495e;}</style>
<div class="container" style="min-height:700px;">

      <div class="section-header">
        <h3 class=" text-center wow fadeInDown animated" style="font-size:35px; visibility: visible; animation-name: fadeInDown;">
          e-NON PNS BADAN KEPEGAWAIAN DAERAH <br>KABUPATEN MANOKWARI SELATAN</h3>
        <h3 class="section-title"></h3>
        <p class="text-center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
          Data Tenaga Non PNS ini berisi informasi pegawai non-pns yang bekerja di lingkungan Kabupaten Manokwari Selatan.
        </p>
    </div>
    <div class=" text-center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
      <div class='alert alert-success' align='center'>
        <i class='fa fa-info-circle'></i>
        Saat ini pengelolaan e-NON PNS dilakukan oleh masing-masing Organisasi Perangkat Daerah agar informasi yang diberikan sesuai dengan data pada setiap OPD<br>                    
      </div>
    </div>


    <div class="row">
        <div class=" text-center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInUp;">
        <div class="col-sm-6">
            <div class="list-group">
              <a href="#" class="list-group-item greenbg">
                Data Tenaga Non PNS 
              </a>  
              <a href="#" class="list-group-item"><span class="badge big"><?= $sum_pegawai->jml ?> </span>Jumlah Non PNS Aktif</a>
              <?php foreach ($jk as $j) { ?>
              <a href="#" class="list-group-item"><span class="badge big"><?= $j->jml ?>  </span>Jumlah Tenaga Non PNS <?= $j->jenis_kelamin ?></a>
              <?php } ?>
            </div>
        </div>
        </div>

        <div class=" text-center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInUp;">
        <div class="col-sm-6">
          <div class="panel panel-default">
            <div class="panel-body "style="margin-left:20px;margin-right:20px;">
                <form action="<?php echo base_url(); ?>pencarian/cari" method="POST" class="form-horizontal" enctype="multipart/form-data"><center>
                  <div class="form-group">
                      <div class="input-group">
                        <span class="input-group-addon ">
                        <i class="fa fa-search"></i></span>
                      <input type="text" required autofocus class="form-control" value="" placeholder="cari pegawai" id="nama" name="nama" ></input>
                      </div>
                      <span align="left" style="color:red">*Masukkan Data Minimal 4 Karakter Huruf</span>
                  </div>
                  <div class="form-group">
                      <select class="form-control select2 select-lg" required id="id_opd" name="id_opd" >
                        <option value="opd">-- Semua OPD --</option>
                        <?php foreach ($all_opd as $op) { ?>
                           <option value="<?= $op->id ?>" ><?= $op->nama ?></option>
                        <?php } ?>                          
                      </select>                     
                  </div>
                  <center><div class="col-sm-12"><button type='submit' name='cari' class='btn  btn-primary'><i class="fa fa-search"></i> Cari Pegawai</button></div></center>
                </form>
            </div>
          </div>
        </div>
        </div>
    </div>
<hr>
    <div class="row">
        <div class=" text-center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInUp;">
        <div class="col-sm-6">
            <div class="list-group">
              <a href="#" class="list-group-item greenbg"><span class="badge big"><?= $sum_opd->jml ?></span>OPD</a>
              <?php $no=0; foreach ($opd as $op) { $no++ ?>  
                <a href="#" class="list-group-item"><span class="badge big"><?= $op->jml ?></span><?= $no ?>. <?= $op->nama ?></a>
              <?php } ?>
            </div>
        </div>
        </div>

        <div class=" text-center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInUp;">
        <div class="col-sm-6">
            <div class="list-group">
              <a href="#" class="list-group-item greenbg"><span class="badge big"><?= $sum_lembaga->jml ?></span>Lembaga/Biro</a>  
              <?php $no=0; foreach ($lembaga as $lg) { $no++ ?>
              <a href="#" class="list-group-item"><span class="badge big"><?= $lg->jml ?></span><?= $no ?>. <?= $lg->nama ?></a>
              <?php } ?>
            </div>
        </div>
        </div>
    </div>

</div>

