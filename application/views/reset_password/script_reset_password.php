<script type="text/javascript">

    $(document).ready(function() {
        $('#password, #password_r').on('input', function() { 
            var p_element = $('#password');
            var p_r_element = $('#password_r');
            if (p_element.val() == p_r_element.val()){
                p_element.css("background","#98f2a3");
                p_r_element.css("background","#98f2a3");
            }else{
                p_element.css("background","#ffc1bc");
                p_r_element.css("background","#ffc1bc");
            }
        });
    });

    $(document).on("submit", "form", function(e){
        var p_element = $('#password');
        var p_r_element = $('#password_r');
        if (p_element.val() != p_r_element.val()){
            e.preventDefault();
            alert('Pengulangan password harus sama');
            return  false;
        }
    });
</script>
</script>