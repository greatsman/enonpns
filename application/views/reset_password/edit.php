
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Reset Password</title>

    <meta name="description" content="User login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="<?=base_url('template/assets/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" href="<?=base_url('template/assets/font-awesome/4.5.0/css/font-awesome.min.css')?>" />

    <!-- text fonts -->
    <link rel="stylesheet" href="<?=base_url('template/assets/css/fonts.googleapis.com.css')?>" />

    <!-- ace styles -->
    <link rel="stylesheet" href="<?=base_url('template/assets/css/ace.min.css')?>" />

    <!--[if lte IE 9]>
      <link rel="stylesheet" href="assets/css/ace-part2.min.css')?>" />
    <![endif]-->
    <link rel="stylesheet" href="<?=base_url('template/assets/css/ace-rtl.min.css')?>" />

    <!--[if lte IE 9]>
      <link rel="stylesheet" href="assets/css/ace-ie.min.css')?>" />
    <![endif]-->

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="login-layout light-login">
    <div class="main-container">
      <div class="main-content">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <div class="login-container" style="padding-top: 100px">
              

              <div class="space-6"></div>
               <?php
                $pesan_sks = $this->session->flashdata('pesan_sks');
                $pesan_ggl = $this->session->flashdata('pesan_ggl');
                ?>
                <?php if (! empty($pesan_sks)) : ?>
                <div class="alert alert-block alert-success">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>
                  <p>
                    <strong>
                      <i class="ace-icon fa fa-check"></i>
                     Berhasil!
                    </strong>
                   <?php echo $pesan_sks ;?>
                  </p>
                </div>
                <?php endif ?>
                <?php if (! empty($pesan_ggl)) : ?>
                <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                      </button>

                      <strong>
                        <i class="ace-icon fa fa-times"></i>
                        Peringatan!
                      </strong>
                      <br>
                       <?php echo $pesan_ggl ;?>
                      <br />
                    </div>
              <?php endif ?>

              <div class="position-relative">
                <div id="login-box" class="login-box visible widget-box no-border">
                  <div class="widget-body">
                    <div class="widget-main">
                      <h4 class="header blue lighter bigger" style="text-align: center;">
                        <i class="ace-icon fa fa-lock red"></i>
                        Reset Password
                      </h4>

                      <div class="space-6"></div>

                      <form action="<?=base_url('reset_password/update');?>" method="post">
                        <fieldset>
                          <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                            		<input type="hidden" id="pidd" name="pidd" value="<?= $pid ?>"/>
		                          <input type="password" class="form-control" id="password" name="password" placeholder="Password baru" />
		                          <i class="ace-icon fa fa-user"></i>
                            </span>
                          </label>

                          <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                              <input type="password" class="form-control" id="password_r" name="password_r" placeholder="Ulangi penulisan" />
                              <i class="ace-icon fa fa-lock"></i>
                            </span>
                          </label>

                          <div class="space"></div>

                          <div class="clearfix">
                            <button type="submit" class="btn btn-sm btn-primary" style="width: 300px">
                              <i class="ace-icon fa fa-key"></i>
                              <span class="bigger-110">Reset</span>
                            </button>
                          </div>

                          <div class="space-4"></div>
                        </fieldset>
                      </form>
                    </div><!-- /.widget-main -->
                  </div><!-- /.widget-body -->
                </div><!-- /.login-box -->
              </div><!-- /.position-relative -->
            </div>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.main-content -->
    </div><!-- /.main-container -->

    <!-- basic scripts -->

    <!--[if !IE]> -->
    <script src="<?=base_url('template/assets/js/jquery-2.1.4.min.js')?>"></script>
    <script src="<?=base_url('template/assets/js/bootstrap.min.js')?>"></script>

    <!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
      <script src="assets/js/excanvas.min.js"></script>
    <![endif]-->
    <script src="<?=base_url('template/assets/js/jquery-ui.custom.min.js')?>"></script>
    <script src="<?=base_url('template/assets/js/jquery.ui.touch-punch.min.js')?>"></script>
    <script src="<?=base_url('template/assets/js/bootbox.js')?>"></script>
    <script src="<?=base_url('template/assets/js/jquery.easypiechart.min.js')?>"></script>
    <script src="<?=base_url('template/assets/js/jquery.gritter.min.js')?>"></script>
    <script src="<?=base_url('template/assets/js/spin.js')?>"></script>

    <!-- ace scripts -->
    <script src="<?=base_url('template/assets/js/ace-elements.min.js')?>"></script>
    <script src="<?=base_url('template/assets/js/ace.min.js')?>"></script>

    <!-- inline scripts related to this page -->
    <script type="text/javascript">
      jQuery(function($) {

      
      });
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
    <script type="text/javascript">
      if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>

    <!-- inline scripts related to this page -->
    <script type="text/javascript">
      jQuery(function($) {

      });
      
      
      
      //you don't need this, just used for changing background
      jQuery(function($) {
       
      });
    </script>
    <script type="text/javascript">

    </script>

	<?php
	  if(isset($scriptExtra)) {
	      echo $scriptExtra;
	  }
	?>
  </body>
</html>
