<?php
if ( ! function_exists('iso_date'))
{
    function iso_date($indo_date) {
        if ($indo_date == "" or $indo_date == "00-00-0000"){
            return "";
        }else {
            return date_format(date_create($indo_date),"Y-m-d");
        }
    }
}