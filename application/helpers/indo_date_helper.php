<?php
if ( ! function_exists('indo_date'))
{
    function indo_date($iso_date) {
    	if ($iso_date == "" or $iso_date == "0000-00-00"){
    		return "";
    	}else {
    		return date_format(date_create($iso_date),"d-m-Y");
    	}
    }
}