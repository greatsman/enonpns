<?php
if ( ! function_exists('normalize_xls_val'))
{
    function normalize_xls_val($cell_val) {
        $str = $cell_val;
        $str = preg_replace("/[^A-Za-z 0-9.-\/&-ʹʺ°]/", '', $str);

        return $str;
    }
}

